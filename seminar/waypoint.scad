$fn = 256;

// Compression tube
color("grey")
difference() {
    rotate([0, 90, 0])
    cylinder(d=8, h=16);
    translate([-1, 0, 0])
    rotate([0, 90, 0])
    cylinder(d=7, h=18);
    rotate([90, 0, 0])
    translate([-1, 0, 0])
    cube([18, 6, 6]);
}

// Markings
color("white")
for (i = [0:2]) {
    translate([6.5 + i, 0, 0]) {
        intersection() {
            translate([-1, 0, -1])
            rotate([-30, 0, 0])
            cube([3, 6, 2]);
            difference() {
                rotate([0, 90, 0])
                cylinder(d=7.5, h=0.5);
                translate([-0.5, 0, 0])
                rotate([0, 90, 0])
                cylinder(d=6.5, h=1.5);
            }
        }
    }
}
/*difference() {
    translate([-1, 0, 0])
    rotate([0, 90, 0])
    cylinder(d=6, h=18);
    rotate([90, 0, 0])
    translate([-1, 0, 0])
    cube([18, 6, 6]);
}*/
