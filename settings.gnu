# Line styles
set style line 1 lt 1 lw 1 pt 1 ps 1 lc rgb "#EDB75C"
set style line 2 lt 1 lw 1 pt 1 ps 1 lc rgb "#645B4B"
set style line 3 lt 1 lw 1 pt 1 ps 1 lc rgb "#D94023"
set style line 4 lt 1 lw 1 pt 1 ps 1 lc rgb "#D94023"

# Delimiter
set datafile separator ","
