// Rods
rh = 1000;
rd = 16;
rg = 64;
module rod() {
    color("grey")
    rotate([90, 0, 90])
    cylinder(d=rd, h=rh, center=true);
}

// Ends
eh = 16;
et = 3;
ez = 32;
module end() {
    color("red")
    rotate([90, 0, 90])
    {
        difference() {
            union() {
                translate([-rg / 2, 0, 0])
                cylinder(d=rd + 2 * et, h=eh);
                translate([+rg / 2, 0, 0])
                cylinder(d=rd + 2 * et, h=eh);
                translate([-rg / 2, -rd / 4 - et / 2, 0])
                cube([rg, rd / 2 + et, eh]);
                translate([-rg / 2 - rd / 4 - et / 2, -ez, 0])
                cube([rd / 2 + et, ez, eh]);
                translate([+rg / 2 - rd / 4 - et / 2, -ez, 0])
                cube([rd / 2 + et, ez, eh]);
            }
            translate([-rg / 2, 0, -1])
            cylinder(d=rd, h=eh + 2);
            translate([+rg / 2, 0, -1])
            cylinder(d=rd, h=eh + 2);
        }
    }
}

// Marker
marker_count = 4;
marker_x = 10;
marker_y = 20;
marker_z = 1;
marker_d = 50;
module marker() {
    color("black") {
        for (i = [-marker_count / 2:marker_count / 2 - 1]) {
            translate([2 * i * marker_x, 0, 0])
            cube([marker_x, marker_y, marker_z]);
        }
    }
}

// Sensor
laser_d = 2;
module sensor() {
    color("red")
    translate([0, -rg / 2, 0])
    rotate([90, 0, 0])
    cylinder(d=laser_d, h=marker_d);
}

// Carriage
ch = 64;
ct = 3.5;
cx = rh * 0.75 - marker_x / 2;
module carriage() {
    color("blue")
    rotate([-90, 0, 90])
    {
        difference() {
            union() {
                translate([-rg / 2, 0, 0])
                cylinder(d=rd + 2 * ct, h=ch);
                translate([+rg / 2, 0, 0])
                cylinder(d=rd + 2 * ct, h=ch);
                translate([-rg / 2, -rd / 2 - ct, 0])
                cube([rg, ct, ch]);
            }
            translate([-rg / 2, 0, -1])
            cylinder(d=rd + 1, h=ch + 2);
            translate([+rg / 2, 0, -1])
            cylinder(d=rd + 1, h=ch + 2);
        }
    }
}
