include <parts.scad>;

$fn = 128;

// Rods
translate([0, -rg / 2, 0])
rod();
translate([0, +rg / 2, 0])
rod();

// Ends
translate([-rh / 2 + 2, 0, 0])
end();
translate([+rh / 2 - eh - 2, 0, 0])
end();

// Carriage
translate([-rh / 2 + ch / 2 + cx, 0, 0]) {
    carriage();
    translate([-ch / 2, 0, rd / 2 + ct + laser_d / 2])
    sensor();
}

// Marker
translate([-rh / 2 + 0.75 * rh, -rg / 2 - rd - marker_d, 0])
marker();
