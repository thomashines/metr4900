include <parts.scad>

$fn = 64;

g = 10;

// Ends
rotate([0, -90, 0])
end();
translate([ez + 2 * g, rg / 2, 0])
rotate([0, -90, 180])
end();

// Carriage
translate([-rd - g, 0, 0])
rotate([0, 90, 0])
carriage();
