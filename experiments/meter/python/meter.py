#!/usr/bin/env python

import matplotlib.pyplot as pyplot
import numpy
import sys

if __name__ == "__main__":
    # Load sensor data
    data = None
    with open("../data/data.5.csv", "rb") as data_file:
        data = numpy.loadtxt(data_file, delimiter=",")
    if data is None:
        sys.exit(1)

    # Load tracker data
    tracker_filenames = [
        "../data/20160511_231342.trk.A.csv",
        "../data/20160511_231342.trk.B.csv",
        "../data/20160511_231342.trk.C.csv",
    ]
    tracker = []
    for tracker_filename in tracker_filenames:
        with open(tracker_filename, "rb") as data_file:
            tracker.append(numpy.loadtxt(data_file, delimiter=","))
    if any(t is None for t in tracker):
        sys.exit(1)

    # Timings
    data_time_start = [
        6,
        17.5,
        25
    ]
    tracker_time_start = [
        25,
        25.82,
        25,
    ]
    checkpoint_times = [
        [0.1955, 0.1985, 0.2015, 0.2044],
        [0.3810, 0.3865, 0.3920, 0.3973],
        [0.3901, 0.3972, 0.4044, 0.4114],
    ]
    impact_time = [
        0.5,
        0.69,
        0.5,
    ]

    # Checkpoint positions
    checkpoint_start = 675E-3;
    checkpoint_positions = \
        numpy.array([0, 1, 2, 3], dtype=numpy.float64) * 10E-3 + \
        checkpoint_start
    print("checkpoint_positions", checkpoint_positions)

    # Experiment to display
    exp = 1

    # Extract arrays
    data_time = data[:, 0] * 1E-6
    # print("data_time", data_time.min(), data_time.max())
    data_accelerometer = data[:, 2]
    # print("data_accelerometer", data_accelerometer.min(),
    #     data_accelerometer.max())
    data_laser = data[:, 1]
    # print("data_laser", data_laser.min(), data_laser.max())
    tracker_time = tracker[exp][:, 0]
    # print("tracker_time", tracker_time.min(), tracker_time.max())
    tracker_position = tracker[exp][:, 1]
    # print("tracker_position", tracker_position.min(), tracker_position.max())

    # Cut to range
    data_range = numpy.logical_and(data_time > data_time_start[exp],
        data_time < data_time_start[exp] + impact_time[exp])
    data_time = data_time[data_range]
    data_laser = data_laser[data_range]
    data_accelerometer = data_accelerometer[data_range]
    # print("data", data_time.shape)

    tracker_range = numpy.logical_and(tracker_time > tracker_time_start[exp],
        tracker_time < tracker_time_start[exp] + impact_time[exp])
    tracker_time = tracker_time[tracker_range]
    tracker_position = tracker_position[tracker_range]
    # print("tracker", tracker_time.shape)

    # Get checkpoint times
    checkpoints = None
    with open("checkpoints.csv", "rb") as checkpoints_file:
        checkpoints = numpy.loadtxt(checkpoints_file, delimiter=",")
    checkpoint_times = None
    if checkpoints is None:
        checkpoint_times = numpy.array(checkpoint_times[exp], dtype=numpy.float64)
    else:
        checkpoint_times = checkpoints[:, 0]

    # Align tracker with checkpoints
    tracker_checkpoint_times = numpy.interp(checkpoint_positions,
        tracker_position, tracker_time)
    tracker_time_adjust = (tracker_checkpoint_times - checkpoint_times).mean()
    # print("tracker_time_adjust", tracker_time_adjust)

    # Save to file
    with open("data.csv", "wb") as data_file:
        numpy.savetxt(data_file,
            numpy.stack([(data_time - data_time.min()) / 35E-6,
                         data_accelerometer,
                         data_laser], axis=1),
            header="time,accelerometer,laser",
            delimiter=",")

    with open("tracker.csv", "wb") as tracker_file:
        numpy.savetxt(tracker_file,
            numpy.stack([tracker_time - tracker_time_adjust,
                         tracker_position], axis=1),
            header="time,position",
            delimiter=",")

    # Plot data
    # data_accelerometer_figure = pyplot.figure()
    # data_accelerometer_axes = data_accelerometer_figure.add_subplot(111)
    # data_accelerometer_axes.set_title("Data acceleration")
    # data_accelerometer_axes.plot(data_time, data_accelerometer)
    # pyplot.show(block=False)

    # data_laser_figure = pyplot.figure()
    # data_laser_axes = data_laser_figure.add_subplot(111)
    # data_laser_axes.set_title("Data laser")
    # data_laser_axes.plot(data_time, data_laser)
    # pyplot.show(block=False)

    # Plot tracker
    # tracker_figure = pyplot.figure()
    # tracker_axes = tracker_figure.add_subplot(111)
    # tracker_axes.set_title("Tracker position")
    # tracker_axes.plot(tracker_time, tracker_position)
    # pyplot.show(block=True)
