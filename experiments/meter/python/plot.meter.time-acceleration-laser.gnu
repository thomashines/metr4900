#!/usr/bin/env gnuplot

# Settings
load "../../../settings.gnu"

# Output
set terminal svg size 1000,500 font "Serif,16"
set output "plot.meter.time-acceleration-laser.svg"

# Multiplot
set multiplot layout 2, 1 title "Prototype measurements"

# Labels
set title "Accelerometer"
set xlabel "Sample number"
set ylabel "MPU-6050 ADC levels"

# Key
set key off

# Axes
#set yrange [-40000:40000]
set ytics 40000
set grid xtics
set grid ytics

# Plot
plot "data.csv" using 1:2           with lines ls 3 title "Accelerometer"

# Labels
set title "Reflection intensity"
set xlabel "Sample number"
set ylabel "ADC levels"

# Axes
set ytics 250
set grid xtics
set grid ytics

# Plot
plot "data.csv" using 1:3 axes x1y2 with lines ls 1 title "Reflection intensity"
