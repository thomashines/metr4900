#!/usr/bin/env python

import matplotlib.pyplot as pyplot
import numpy

if __name__ == "__main__":
    # Load output
    calibrated = None
    with open("calibrated.csv", "rb") as calibrated_file:
        calibrated = numpy.loadtxt(calibrated_file, delimiter=",")
    if calibrated is None:
        print("couldn't load calibrated")
        sys.exit(1)

    # Load truth
    truth = None
    with open("tracker.csv", "rb") as truth_file:
        truth = numpy.loadtxt(truth_file, delimiter=",")
    if truth is None:
        print("couldn't load truth")
        sys.exit(1)


    # Find matching region
    time_start = max(calibrated[:, 0].min(), truth[:, 0].min())
    time_end = min(calibrated[:, 0].max(), truth[:, 0].max())

    # Interpolate calibrated at truth times
    truth_region = numpy.logical_and(truth[:, 0] > time_start,
        truth[:, 0] < time_end)
    calibrated_at_truth = numpy.interp(truth[truth_region, 0],
        calibrated[:, 0], calibrated[:, 3])

    # Find difference
    error = calibrated_at_truth - truth[truth_region, 1]
    error_time = truth[truth_region, 0]
    impact_time = 0.55
    print("max error", numpy.absolute(error[error_time < impact_time]).max())

    # Plot
    # pyplot.plot(calibrated[:, 0], calibrated[:, 3])
    # pyplot.plot(truth[:, 0], truth[:, 2])
    # pyplot.plot(truth[truth_region, 0], numpy.absolute(error))
    # pyplot.show()

    # Save to csv file
    with open("error.csv", "wb") as error_file:
        numpy.savetxt(error_file,
            numpy.stack([truth[truth_region, 0],
                         calibrated_at_truth,
                         truth[truth_region, 1],
                         numpy.absolute(error)], axis=1),
            header="time,calibrated,true,error",
            delimiter=",")
