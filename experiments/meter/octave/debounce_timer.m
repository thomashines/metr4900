function [y] = debounce_timer(x)
    y = x;
    last_state = y(1);
    bouncing = false;
    edge_at = 1;
    debounce_period = 16;
    for n = 2:(size(y, 1) - 1)
        % Check for edge
        if y(n) ~= last_state
            last_state = y(n);
            % Check if settled
            if ~bouncing
                bouncing = true;
                edge_at = n;
            end
        end
        % Check if settled
        if bouncing
            if (n - edge_at) > debounce_period
                y(edge_at:n) = y(n);
                bouncing = false;
            end
        end
    end
end
