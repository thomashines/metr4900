function [position_time, position, acceleration] = reconstruct( ...
        theta, train_time, sensor_time, accelerometer, padding);
    % Accelerometer synchronisation
    position_time = sensor_time - theta(1);

    % Accelerometer cutting
    % s = min(train_time) - padding
    % e = max(train_time') + padding
    position_time_valid = ( ...
        position_time > (min(train_time) - padding) & ...
        position_time < (max(train_time) + padding));
    position_time = position_time(position_time_valid);
    accelerometer_cut = accelerometer(position_time_valid);

    % Get acceleration
    acceleration = accelerometer_cut ./ theta(2);
    acceleration = acceleration - theta(3);

    % Derive sensor position
    speed = zeros(size(acceleration));
    position = zeros(size(acceleration));
    for n = 1:(size(acceleration, 1) - 1)
        dt = position_time(n + 1) - position_time(n);
        speed(n + 1) = speed(n) + ...
            acceleration(n) * dt;
        position(n + 1) = position(n) + ...
            speed(n) * dt + ...
            acceleration(n) * (dt ^ 2) * 0.5;
    end
end
