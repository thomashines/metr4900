close all; clear all;
% pkg load signal;
% pkg load financial;

% Load tracker data
tracker_data = {
    csvread('../data/20160511_231342.trk.A.csv', 2, 0),
    csvread('../data/20160511_231342.trk.B.csv', 2, 0),
    csvread('../data/20160511_231342.trk.C.csv', 2, 0),
};

% Load sensor data
sensor_data = csvread('../data/data.5.csv');
sensor_time = sensor_data(:, 1);
sensor_timescale = 1E+6; % us
sensor_time = sensor_time ./ sensor_timescale;

% Trial inputs
trials = size(tracker_data, 1);

% Acceleration
accelerometer = sensor_data(:, 3);

% Intensity
intensity = sensor_data(:, 2);
intensity_time_start = {
    6.24,
    17.63,
    25.29,
};

% Impact
impact_time = {0.2426, 0.4186, 0.4939};

% Waypoint extraction
intensity_edges = {
    [0.1955, 0.1985, 0.2015, 0.2044, 0.2074, 0.2103, 0.2132, 0.2176],
    [0.3425, 0.3478, 0.3532, 0.3583, 0.3638, 0.3695, 0.3748, 0.3810],
    [0.3901, 0.3972, 0.4044, 0.4114, 0.4186, 0.4268, 0.4341, 0.4423],
};
intensity_edges_count = size(intensity_edges{1});
intensity_edges_adjust = {-0.0005, -0.0122, -0.0175};

% Waypoint positions
edge_start = 657E-3;
edge_x = edge_start + [0, 1, 2, 3, 4, 5, 6, 7] .* 10E-3;

% Calibration
% calibrate = true;
calibrate = false;
accelerometer_time_start = {
     6.2336,
    %  6,
    17.6260,
    % 17,
    25.2891,
    % 25,
};
accelerometer_sensitivity = {
    % 200.0665,
    200,
    % 200.0104,
    200,
    200.0172,
    % 200,
};
accelerometer_mean = {
    % 5.3290,
    4.8,
    % 4.9804,
    4.8,
    % 4.8588,
    5.1748,
    % 4.8,
};

% for trial = 1:trials
% for trial = 1:1
for trial = 2:2
% for trial = 3:3
    % Extract tracker data
    tracker_time = tracker_data{trial}(3:end, 1);
    tracker_x = tracker_data{trial}(3:end, 2);

    % Synchronise with tracker
    tracker_time = tracker_time - tracker_time(1);

    % Cut tracker time
    padding = 0.05;
    tracker_time_trial = tracker_time < (impact_time{trial} + padding);
    tracker_time = tracker_time(tracker_time_trial);
    tracker_x = tracker_x(tracker_time_trial);

    % Derive tracker acceleration
    tracker_u = zeros(size(tracker_x));
    tracker_a = zeros(size(tracker_x));
    for n = 1:(size(tracker_x, 1) - 1)
        dt = tracker_time(n + 1) - tracker_time(n);
        tracker_u(n + 1) = (tracker_x(n + 1) - tracker_x(n)) / dt;
        tracker_a(n + 1) = (tracker_u(n + 1) - tracker_u(n)) / dt;
    end

    % Get waypoints
    intensity_edges{trial} = intensity_edges{trial} + intensity_edges_adjust{trial};
    waypoint_time = [0, intensity_edges{trial}, impact_time{trial}];
    impact_x = interp1(tracker_time, tracker_x, impact_time{trial}, 'spline');
    waypoint_x = [0, edge_x, impact_x];

    % Calibrate accelerometer

    % Objective: minimise difference between integrated position and waypoint
    %   position at the waypoint times

    % Variables:
    %   accelerometer time value at t = 0
    %   accelerometer sensitivity
    %   accelerometer mean
    theta = [
        accelerometer_time_start{trial}, ...
        accelerometer_sensitivity{trial}, ...
        accelerometer_mean{trial}, ...
    ];
    if calibrate
        % [J, dJ] = cost( ...
        [J] = cost( ...
            theta, ...
            waypoint_time, ...
            waypoint_x, ...
            sensor_time, ...
            accelerometer, ...
            padding ...
        );
        % 'Display', 'iter' ...
        options = optimset( ...
            'GradObj', 'off', ...
            'TolX', 1E-100, ...
            'MaxFunEvals', 1E+3 ...
        );
        % Conditions
        A = [];
        b = [];
        Aeq = [];
        beq = [];
        lb = [
            accelerometer_time_start{trial} - 1, ...
            100, ...
            -10, ...
        ];
        ub = [
            accelerometer_time_start{trial} + 1, ...
            300, ...
            +10, ...
        ];
        nonlcon = [];
        [theta, J, exitflag, output] = fmincon( ...
            @(theta) cost( ...
                theta, ...
                waypoint_time, ...
                waypoint_x, ...
                sensor_time, ...
                accelerometer, ...
                padding ...
            ), ...
            theta, ...
            A, b, ...
            Aeq, beq, ...
            lb, ub, ...
            nonlcon, ...
            options);
        % [theta, J] = fminsearch( ...
        % [theta, J, info, output] = fminunc( ...
        %     @(theta) cost( ...
        %         theta, ...
        %         waypoint_time, ...
        %         waypoint_x, ...
        %         sensor_time, ...
        %         accelerometer, ...
        %         padding ...
        %     ), ...
        %     theta, ...
        %     options);
        newtheta = theta
        J = J
        exitflag = exitflag
        % output = output
    end

    % Get calibration
    accelerometer_time_start{trial} = theta(1);
    accelerometer_sensitivity{trial} = theta(2);
    accelerometer_mean{trial} = theta(3);

    % Get position
    [position_time, position, acceleration] = reconstruct( ...
        theta, tracker_time, sensor_time, accelerometer, padding);

    % Sample tracker
    % tracker_x = interp1(tracker_time, tracker_x, position_time, 'spline');

    % Plot acceleration
    % acceleration_plot = figure(1); hold on;
    % plot(tracker_time, tracker_a, 'g', 'LineWidth', 2);
    % plot(position_time, acceleration, 'r', 'LineWidth', 2);
    % title('Acceleration');
    % xlabel('Time (s)');
    % ylabel('Acceleration (m*s^-2)');
    % legend( ...
    %     'Tracker acceleration', ...
    %     'Measured acceleration', ...
    %     'location', 'northwest');
    % print -dpng -color '-S900,450' acceleration.png;

    % Plot position
    position_plot = figure(2); hold on;
    % plot(position_time, tracker_x, 'g', 'LineWidth', 2);
    plot(tracker_time, tracker_x, 'g', 'LineWidth', 2);
    plot(position_time, position, 'r', 'LineWidth', 2);
    plot(waypoint_time, waypoint_x, 'kx', 'LineWidth', 2);
    title('Position');
    xlabel('Time (s)');
    ylabel('Position (m)');
    legend( ...
        'Tracker position', ...
        'Measured position', ...
        'Waypoint positions', ...
        'location', 'northwest');
    % print -dpng -color '-S900,450' position.png;

    % Plot error
    % error_plot = figure(2); hold on;
    % plot(accelerometer_time, tracker_x, 'g', 'LineWidth', 2);
    % plot(accelerometer_time, position, 'r', 'LineWidth', 2);
    % % axis([0, 0.52, 0, 0.85]);
    % title('Position error correction');
    % xlabel('Time (s)');
    % ylabel('Position (m)');
    % legend( ...
    %     'Tracker position', ...
    %     'Estimated position', ...
    %     'location', 'northwest');
    % print -dpng -color '-S900,450' error.png;
end
