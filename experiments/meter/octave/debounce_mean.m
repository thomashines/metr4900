function [y] = debounce_mean(x, len)
    y = zeros(size(x));
    for n = (1 + len):(size(y, 1) - len)
        y(n) = mean(x((n - len):(n + len))) > 0.5;
    end
end
