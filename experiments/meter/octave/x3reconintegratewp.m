function [xss, uss, ass] = x3reconintegratewp(tss, ass, waypoint_t, waypoint_x)
    % Integrate for velocity
    uss = zeros(size(tss));
    for n = 1:(size(tss, 1) - 1)
        dt = tss(n + 1) - tss(n);
        uss(n + 1) = uss(n) + ass(n) * dt;
    end

    % Integrate for position
    xss = zeros(size(tss));
    for n = 1:(size(tss, 1) - 1)
        dt = tss(n + 1) - tss(n);
        xss(n + 1) = xss(n) + uss(n) * dt + ass(n) * (dt ^ 2) / 2;
    end

    xss_raw = xss;

    % Get estimated position at waypoints
    xss_waypoint_t = interp1(tss, xss, waypoint_t);

    % Get linear shift
    diff = waypoint_x - xss_waypoint_t;
    diff_tss = interp1(waypoint_t, diff, tss);

    % Plot
    figure; hold on;
    % plot(waypoint_t, diff, 'r');
    plot(tss, xss_raw, 'b');
    xlabel('Time (s)');
    ylabel('Position (m)');

    % Correct position
    xss = xss + diff_tss;
end
