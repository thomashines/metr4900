function [J] = cost(theta, train_time, train_x, sensor_time, accelerometer, padding)
% function [J, dJ] = cost(theta, train_time, train_x, sensor_time, accelerometer, padding)
    J = 0;
    dJ = zeros(size(theta));
    % theta = theta

    % Get position
    [position_time, position, acceleration] = reconstruct(theta, train_time, ...
        sensor_time, accelerometer, padding);

    % ltt = min(train_time)
    % htt = max(train_time)
    % ltt = min(position_time)
    % htt = max(position_time)

    if size(position_time, 1) < 2 || ...
            min(train_time) < min(position_time) || ...
            max(train_time) > max(position_time)
        J = 100;
    else
        % Sample position at training times
        position_train_time = interp1(position_time, position, train_time, 'spline');

        % Calculate error
        J = sum((train_x - position_train_time) .^ 2);
    end
end
