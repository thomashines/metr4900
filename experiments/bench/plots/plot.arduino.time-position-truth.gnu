#!/usr/bin/env gnuplot

# Settings
load "../../../settings.gnu"

# Output
set terminal svg size 1000,500 font "Serif,16"
set output "plot.arduino.time-position-truth.svg"

# Key
set key center right

# Labels
set title "Reconstructed trajectory vs. true trajectory for bench test"
set xlabel "Time (s)"
set ylabel "Position (m)"
set y2label "Error (m)"

# Axes
set y2tics
set y2tics 0.006
set ytics nomirror
set grid xtics
set grid ytics

# Plot
set multiplot
plot "arduino_error.csv" using 1:3           with  lines ls 2 lc rgb "#BB645B4B" lw 8 title "True position", \
     "arduino_error.csv" using 1:2           with  lines ls 2                         title "Estimated position", \
     "arduino_error.csv" using 1:4 axes x1y2 with  lines ls 6                         title "Error", \
       "checkpoints.csv" using 1:2           with points ls 1                         title "Checkpoints"
