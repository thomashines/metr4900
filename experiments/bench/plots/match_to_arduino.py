#!/usr/bin/env python

import matplotlib.pyplot as pyplot
import numpy
import sys

# Arduino config
spool_diameter = 22.5E-3
ticks_per_revolution = 24
ticks_per_meter = ticks_per_revolution / (numpy.pi * spool_diameter)
print("ticks_per_meter", ticks_per_meter)

# First 50mm: 17 ticks (0.34 ticks/mm)
# Last 50mm: 17 ticks (0.34 ticks/mm)

# First 100mm: 33 ticks (0.33 ticks/mm)
# Last 100mm: 35 ticks (0.35 ticks/mm)

# 250mm: 81, 81, 81 (0.324 ticks/mm)
# 500mm: 166, 164, 165 (0.330 ticks/mm)
# 750mm: 252, 251, 251 (0.335 ticks/mm)

m = [
    [81, 250E-3],
    [165, 500E-3],
    [251, 750E-3],
]

f = 1
t = 2

g = (m[f][1] * (m[t][0] ** 2) - m[t][1] * (m[f][0] ** 2)) / \
    ((m[t][0] ** 2) * m[f][0] - (m[f][0] ** 2) * m[t][0])
c = (m[t][1] - g * m[t][0]) / (m[t][0] ** 2)
print("c", c)
print("g", g)

def ticks_to_m(ticks):
    return c * (ticks ** 2) + g * ticks

if __name__ == "__main__":
    print("X3 Accelerometer: compare real and measured trajectories")

    # Check arguments
    if len(sys.argv) != 4:
        print("Usage: ./match_to_arduino.py <calibrated.csv> <checkpoints.csv> <arduino.csv>")

    # Load data
    with open(sys.argv[1], "rb") as calibrated_file:
        calibrated = numpy.loadtxt(calibrated_file, delimiter=",")
    calibrated_time = calibrated[:, 0]
    calibrated_position = calibrated[:, 3]
    with open(sys.argv[2], "rb") as checkpoints_file:
        checkpoints = numpy.loadtxt(checkpoints_file, delimiter=",")
    checkpoint_times = checkpoints[:, 0]
    checkpoint_positions = checkpoints[:, 1]
    with open(sys.argv[3], "rb") as arduino_file:
        arduino = numpy.loadtxt(arduino_file, delimiter=",")
    arduino_time = arduino[:, 1] * 1E-6
    # arduino_position = arduino[:, 0] / ticks_per_meter
    arduino_position = numpy.apply_along_axis(ticks_to_m, 0, arduino[:, 0])

    # Get arduino time adjustment
    arduino_time_at_checkpoints = numpy.interp(checkpoint_positions,
        arduino_position, arduino_time)
    arduino_time_adjust = \
        (arduino_time_at_checkpoints - checkpoint_times).mean()
    print("arduino_time_adjust", arduino_time_adjust)

    # Adjust arduino time
    arduino_time = arduino_time - arduino_time_adjust

    # Calculate error

    # Find matching region
    time_start = max(calibrated_time.min(), arduino_time.min())
    time_end = min(calibrated_time.max(), arduino_time.max())
    print("calibrated_time", calibrated_time.min(), calibrated_time.max())
    print("arduino_time", arduino_time.min(), arduino_time.max())
    print("time", time_start, time_end)

    # Interpolate calibrated at arduino times
    arduino_region = numpy.logical_and(arduino_time > time_start,
        arduino_time < time_end)
    calibrated_at_arduino = numpy.interp(arduino_time[arduino_region],
        calibrated_time, calibrated_position)

    # Find difference
    error = calibrated_at_arduino - arduino_position[arduino_region]
    print("max error", numpy.absolute(error).max())

    # Save data
    with open("arduino_error.csv", "wb") as error_file:
        numpy.savetxt(error_file,
            numpy.stack([arduino_time[arduino_region],
                         calibrated_at_arduino,
                         arduino_position[arduino_region],
                         numpy.absolute(error)], axis=1),
            header="time,calibrated,arduino,error",
            delimiter=",")
