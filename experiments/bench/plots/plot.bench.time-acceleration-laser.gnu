#!/usr/bin/env gnuplot

# Settings
load "../../../settings.gnu"

# Output
set terminal svg size 1000,500 font "Serif,16"
set output "plot.bench.time-acceleration-laser.svg"

# Multiplot
set multiplot layout 2, 1 title "Bench test measurements"

# Labels
set title "Accelerometer"
set xlabel "Sample number"
set ylabel "ADC levels"

# Key
set key off

# Axes
set ytics 500
set grid xtics
set grid ytics

# Plot
plot "../data/data.23.csv" using 1:2 with lines ls 3 title "Accelerometer"

# Labels
set title "Reflection intensity"
set xlabel "Sample number"
set ylabel "ADC levels"

# Axes
set yrange [3750:4000]
set ytics 125
set grid xtics
set grid ytics

# Plot
plot "../data/data.23.csv" using 1:3 with lines ls 1 title "Reflection intensity"
