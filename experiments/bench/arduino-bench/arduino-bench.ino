#define X_STEP_PIN         54
#define X_DIR_PIN          55
#define X_ENABLE_PIN       38
#define X_MIN_PIN           3
#define X_MAX_PIN           2
#define X_FORWARD           LOW
#define X_BACKWARD          HIGH

void setup() {
    // Setup pins
    pinMode(X_STEP_PIN, OUTPUT);
    pinMode(X_DIR_PIN, OUTPUT);
    pinMode(X_ENABLE_PIN, OUTPUT);
    pinMode(X_MIN_PIN, OUTPUT);
    pinMode(X_MAX_PIN, OUTPUT);

    // Enable steppers
    digitalWrite(X_ENABLE_PIN, LOW);
    digitalWrite(X_DIR_PIN, X_FORWARD);
    digitalWrite(X_STEP_PIN, LOW);

    // Serial
    Serial.begin(115200);

    // Wait for A4988
    delayMicroseconds(1);
}

// Floating point
#define ACC_0_LEN 0.01
#define ACC_0_LEN_US ((unsigned long) round(((float) ACC_0_LEN) * ((float) 1E+6)))
#define ACC_0_AMP 5.0
#define ACC_1_LEN 5.0
#define ACC_1_LEN_US ((unsigned long) round(((float) ACC_1_LEN) * ((float) 1E+6)))
#define ACC_1_AMP -0.0
#define SINE_AMP 10.0
#define SINE_LEN 0.15
#define SINE_LEN_US ((unsigned long) round(((float) SINE_LEN) * ((float) 1E+6)))
#define ACC_FAC_0 0.0
#define ACC_FAC_1 -1.5
#define ACC_FRICTION (SINE_LEN * ACC_FAC_1)
#define TEST_LEN 0.5
#define TICKS_PER_REV 200.0
#define SPOOL_DIAM 0.1
#define PI 3.141592653589793
// TICKS_PER_M = 2893.7
// 0.75 m in 2170
#define TICKS_PER_M (TICKS_PER_REV / (SPOOL_DIAM * PI))
#define FLOAT_TOL 1E-3
#define PRINT_PERIOD_US 25000

// Path state
bool go;
unsigned long start_us, before_us, now_us, last_print_us, loops;
double start, before, now, dt, acceleration, velocity, position;

// Stepper state
long target_ticks, actual_ticks;
int tick_step;

void print_state() {
    Serial.print(micros() - start_us); Serial.print(",");
    // Serial.print(now, 4); Serial.print(",");
    // Serial.print(acceleration, 4); Serial.print(",");
    // Serial.print(velocity, 4); Serial.print(",");
    // Serial.print(position, 4); Serial.print(",");
    Serial.println(actual_ticks);
}

void loop() {
    // Wait for serial
    go = false;
    while (!go) {
        if (Serial.available()) {
            if (Serial.read() == 'y') {
                go = true;
            }
        }
    }

    // Clear state
    start_us = micros();
    start = start * 1E-6;
    before_us = 0;
    before = before_us * 1E-6;
    now_us = 0;
    now = now_us * 1E-6;
    last_print_us = 0;
    loops = 0;
    dt = 0.0;
    acceleration = 0.0;
    velocity = 0.0;
    position = 0.0;
    target_ticks = 0;
    actual_ticks = 0;
    tick_step = 0;

    // Print initial state
    Serial.println("#start");
    Serial.print("#TICKS_PER_M "); Serial.println(TICKS_PER_M, 4);
    Serial.print("#SINE_LEN_US "); Serial.println(SINE_LEN_US);
    print_state();

    // Generate path
    while(go) {
        // Get current time
        now_us = micros() - start_us;
        now = now_us * 1E-6;

        // Get current acceleration
        acceleration = 0.0;

        // Sinusoid
        // if (now_us <= SINE_LEN_US) {
        //     // Sine component
        //     acceleration +=
        //         (SINE_AMP / 2.0) * (1.0 - cos((2.0 * PI / SINE_LEN) * now));
        //
        //     // Linear component
        //     acceleration += ACC_FAC_0 + ACC_FAC_1 * now;
        // } else {
        //     // Friction
        //     acceleration += ACC_FRICTION;
        // }

        // Constant
        if (now_us <= ACC_0_LEN_US) {
            acceleration += ACC_0_AMP;
        } else if (now_us <= ACC_1_LEN_US) {
            acceleration += ACC_1_AMP;
        }

        // Get change in time
        dt = now - before;

        // Adjust velocity
        velocity += acceleration * dt;

        // Adjust position
        position += velocity * dt;

        // Convert position into ticks
        target_ticks = (long) round(position * TICKS_PER_M);

        // Step toward target
        if (target_ticks != actual_ticks) {
            if (target_ticks > actual_ticks) {
                // Step forward
                digitalWrite(X_DIR_PIN, X_FORWARD);
                tick_step = +1;
            } else {
                // Step backward
                digitalWrite(X_DIR_PIN, X_BACKWARD);
                tick_step = -1;
            }
            delayMicroseconds(1);
            while (target_ticks != actual_ticks) {
                // Step
                digitalWrite(X_STEP_PIN, HIGH);
                delayMicroseconds(2);
                digitalWrite(X_STEP_PIN, LOW);
                delayMicroseconds(2);
                actual_ticks += tick_step;
            }
        }

        if (now_us - last_print_us >= PRINT_PERIOD_US) {
            last_print_us = now_us;
            // Print state
            print_state();
        }

        // Copy previous time
        before_us = now_us;
        before = now;

        // Check for end
        go = position >= -FLOAT_TOL && position <= TEST_LEN;
        loops++;
    }

    // Print final state
    print_state();

    // Announce completion
    Serial.print("#stopped in "); Serial.print(loops); Serial.println(" loops");
    // Serial.print("now "); Serial.println(now);
    // Serial.print("acceleration "); Serial.println(acceleration);
    // Serial.print("velocity "); Serial.println(velocity);
    // Serial.print("position "); Serial.println(position);
    // Serial.print("loops "); Serial.println(loops);
}
