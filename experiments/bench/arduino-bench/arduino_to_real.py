#!/usr/bin/env python

import numpy
import sys

if __name__ == "__main__":
    # Load Arduino
    arduino = numpy.loadtxt(sys.argv[1], delimiter=",")

    # Convert to real
    real = numpy.zeros(arduino.shape)
    real[:,0] = arduino[:,0] * 1E-6
    real[:,1] = arduino[:,1] / 2798.3286

    # Save
    numpy.savetxt("real.csv", real, delimiter=",")
