#!/usr/bin/env gnuplot

# Output
set terminal svg size 1000, 500 font "HelveticaNeue-Thin,12"
set output "plot.time-position.svg"

# Key
set tmargin at screen 0.9
set rmargin at screen 0.95
set bmargin at screen 0.2
set lmargin at screen 0.1
#set key bmargin left
set key at 0.3,-0.1
set key box black
set key horizontal
#set key off

# Labels
set title "Calibrated trajectory" font "HelveticaNeue-Thin,16" offset 0,-0.8
set xlabel "Time (s)"
set ylabel "Position (m)"
set y2label "Acceleration (m/s/s)"

# Axes
set tics out
set grid ytics
set y2tics
set ytics nomirror
#set xrange [0:0.7831]
#set yrange [0:0.6308]

# Plot
set multiplot
plot "data.1.csv" using 1:4 with lines ls 1 title "Position", \
     "data.1.csv" using 1:2 axes x1y2 with lines ls 2 title "Acceleration", \
     "real.csv" using 1:2 with lines ls 4 title "Real position"
