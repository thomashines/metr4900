pcb_xy = 50;
pcb_bolt_xy = 42;
pcb_bolt_dxy = (pcb_xy - pcb_bolt_xy) / 2;
bolt_d = 3.2;
standoff_d = 6;

module pcb(t=1.5) {
    color("green")
    difference() {
        translate([-pcb_xy / 2, -pcb_xy / 2, 0])
        cube([pcb_xy, pcb_xy, t]);
        for (m = [[0, 0, 0],
                  [0, 1, 0],
                  [1, 0, 0],
                  [1, 1, 0]]) {
            mirror(m)
            translate([-pcb_bolt_xy / 2, -pcb_bolt_xy / 2, -1])
            cylinder(d=bolt_d, h=t + 2);
        }
    }
}

module standoff(h, od=standoff_d, id=bolt_d) {
    color("grey")
    difference() {
        cylinder(d=od, h=h);
        translate([0, 0, -1])
        cylinder(d=id, h=h + 2);
    }
}

module stack(i, pcb_h, pcb_dz) {
    pcb(pcb_h[i]);
    if (i < len(pcb_h) - 1) {
        translate([0, 0, pcb_h[i]]) {
            for (m = [[0, 0, 0],
                      [0, 1, 0],
                      [1, 0, 0],
                      [1, 1, 0]]) {
                mirror(m)
                translate([-pcb_bolt_xy / 2, -pcb_bolt_xy / 2, 0])
                standoff(pcb_dz[i + 1]);
            }
            translate([0, 0, pcb_dz[i + 1]])
            stack(i + 1, pcb_h, pcb_dz);
        }
    }
}

// PCB stack
pcb_h = [1.4, 1.3, 1.4, 1.5];
pcb_dz = [0, 25, 15, 12];
stack(0, pcb_h, pcb_dz);

// Top plate
plate_d = 75;
plate_h = 6.5;
stack_h = 89.1;
module top_plate() {
    difference() {
        union() {
            difference() {
                cylinder(d=plate_d, h=plate_h);
                translate([0, 0, -1])
                cylinder(d=plate_d - plate_h * 2, h=plate_h + 2);
            }
            for (r = [[0, 0, 45], [0, 0, -45]]) {
                rotate(r)
                translate([0, 0, plate_h / 2])
                cube([plate_d - plate_h, plate_h, plate_h], center=true);
            }
            /*for (m = [[0, 0, 0],
                      [0, 1, 0],
                      [1, 0, 0],
                      [1, 1, 0]]) {
                mirror(m)
                translate([-pcb_bolt_xy / 2, -pcb_bolt_xy / 2, 0])
                cylinder(d=standoff_d, h=plate_h);
            }*/

            // Mounting
            for (r = [[0, 0, 0], [0, 0, -90], [0, 0, 180]]) {
                rotate(r)
                translate([(plate_d - 2 * plate_h) / 2, 0, 0])
                for (m = [[0, 0, 0], [0, 1, 0]]) {
                    mirror(m)
                    translate([0, 15, 0])
                    difference() {
                        translate([0, -plate_h / 2, 0])
                        cube([2 * plate_h, plate_h, plate_h]);
                        translate([plate_h * (3 / 2), 0, -1])
                        cylinder(d=bolt_d, h=plate_h + 2);
                    }
                }
            }
        }
        translate([-plate_d / 2 - 1, cos(45) * plate_d / 2, -1])
        cube([plate_d + 2, plate_d + 2, plate_h + 2]);
        for (m = [[0, 0, 0],
                  [0, 1, 0],
                  [1, 0, 0],
                  [1, 1, 0]]) {
            mirror(m)
            translate([-pcb_bolt_xy / 2, -pcb_bolt_xy / 2, -1])
            cylinder(d=bolt_d, h=plate_h + 2);
        }
        rotate([0, 0, 45])
        translate([plate_h / 2, plate_h / 2, -1])
        cube([plate_d, plate_d, plate_h + 2]);
    }
}
translate([0, 0, stack_h - plate_h]) {
    top_plate();
    for (m = [[0, 0, 0],
              [0, 1, 0],
              [1, 0, 0],
              [1, 1, 0]]) {
        mirror(m)
        translate([-pcb_bolt_xy / 2, -pcb_bolt_xy / 2, -25])
        standoff(25);
    }
}

// Bottom plate
acc_d = 30;
acc_bolt_d = 1.6;
acc_bolt_dx = 3.81;
module bottom_plate() {
    difference() {
        union() {
            top_plate();
            cylinder(d=acc_d, h=plate_h);
            translate([-plate_h / 2, 0, 0])
            cube([plate_h, cos(45) * plate_d / 2, plate_h]);
        }
        for (m = [[0, 0, 0], [1, 0, 0]]) {
            mirror(m)
            translate([acc_bolt_dx, 0, -1])
            cylinder(d=acc_bolt_d, h=plate_h + 2);
        }
    }
}
translate([0, 0, -15 - plate_h]) {
    bottom_plate();
    for (m = [[0, 0, 0],
              [0, 1, 0],
              [1, 0, 0],
              [1, 1, 0]]) {
        mirror(m)
        translate([-pcb_bolt_xy / 2, -pcb_bolt_xy / 2, plate_h])
        standoff(15);
    }
}
