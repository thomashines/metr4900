use <stack.scad>

$fn = 32;

standoff_d = 6;
standoffs = [25, 25, 15, 15, 12];

for (m = [[0, 0, 0],
          [0, 1, 0],
          [1, 0, 0],
          [1, 1, 0]]) {
    mirror(m)
    translate([-standoff_d * (2 / 3), -standoff_d * (2 / 3), 0])
    standoff(standoffs[0]);
}

for (i = [1:len(standoffs) - 1]) {
    rotate([0, 0, 90 * i])
    translate([(8 / 3) * standoff_d, 0, 0]) {
        for (m = [[0, 0, 0],
                  [0, 1, 0],
                  [1, 0, 0],
                  [1, 1, 0]]) {
            mirror(m)
            translate([-standoff_d * (2 / 3), -standoff_d * (2 / 3), 0])
            standoff(standoffs[i]);
        }
    }
}
