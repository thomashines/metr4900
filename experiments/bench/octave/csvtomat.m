% Load data
data_csv = csvread("../data/data.17.csv");

% Extract vectors
sample_number = data_csv(:, 1);
data_acc = data_csv(:, 2);
data_las = data_csv(:, 3);

% Scale time
sample_period_us = 35;
data_time = sample_number * sample_period_us * 1E-6;

% Look for outliers
for n = 1:size(sample_number, 1) - 1
    if sample_number(n) > sample_number(n + 1)
        disp(sprintf("outlier@%d: %d to %d", n, sample_number(n), sample_number(n + 1)));
    end
end

% Crop
% crop_start = 0.8
% crop_end = 2.0
% crop_region = data_time >= crop_start & data_time <= crop_end;
% data_time = data_time(crop_region);
% data_time -= data_time(1);
% data_acc = data_acc(crop_region);
% data_las = data_las(crop_region);

% Save data
save("-mat-binary", "data.mat", "data_time", "data_acc", "data_las");
csvwrite("data.csv", [data_time, data_acc, data_las]);
