clear all; close all;

% Load data
load("data.mat");

% Checkpoint crossings
checkpoint_time = [0.7250; 0.7284; 0.7316; 0.7349];
checkpoint_pos = 0.75 + [0; 1; 2; 3] .* 0.005;
save("-mat-binary", "checkpoints.mat", "checkpoint_time", "checkpoint_pos");
csvwrite("checkpoints.csv", [checkpoint_time, checkpoint_pos]);

% Plot laser data
laser_start = 0.72;
% laser_start = 0.0;
laser_end = 0.75;
% laser_end = 2.0;
laser_region = data_time >= laser_start & data_time <= laser_end;
figure; hold on;
plot(data_time(laser_region), data_las(laser_region));
for t = checkpoint_time
    plot([1, 1] .* t, [min(data_las), max(data_las)]);
end

% Plot acceleration data
acc_start = 0.0;
acc_end = 5.0;
acc_region = data_time >= acc_start & data_time <= acc_end;
figure; hold on;
plot(data_time(acc_region), data_acc(acc_region));
