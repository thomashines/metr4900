#!/usr/bin/env python

import numpy
import serial
import sys
import time

# Settings
sample_period = 1e-3
# MS=0b000: 200 / (22 * pi) = 2.8937
# MS=0b100: 400 / (22 * pi) = 5.7875
ticks_per_revolution = 400
spool_diameter = 22e-3
ticks_per_m = ticks_per_revolution / (spool_diameter * numpy.pi)
print("ticks_per_m", ticks_per_m)
acceleration_factors = [0, -9]
test_length = 0.790
sine_length = 0.1
sine_amplitude = 25

def acceleration_function(time):
    acceleration = 0

    # Sine component
    if time <= sine_length:
        acceleration += (sine_amplitude / 2) * (1 - numpy.cos((2 * numpy.pi / sine_length) * time))

    # Linear component
    if time <= sine_length:
        acceleration += sum([acceleration_factors[i] * (time ** i) \
            for i in range(len(acceleration_factors))])
    else:
        acceleration += sum([acceleration_factors[i] * (sine_length ** i) \
            for i in range(len(acceleration_factors))])

    # acceleration += sum([acceleration_factors[i] * (time ** i) \
    #     for i in range(len(acceleration_factors))])

    return acceleration

def velocity_to_micros(velocity):
    if velocity is None or velocity == 0:
        return 0
    return int((1e6 / ticks_per_m) / velocity)

def micros_to_velocity(micros):
    if micros is None or micros == 0:
        return 0
    return (1e6 / ticks_per_m) / micros

if __name__ == "__main__":
    time_vector = numpy.zeros(1)
    acceleration = numpy.zeros(1)
    velocity = numpy.zeros(1)
    position = numpy.zeros(1)

    # Initial acceleration
    acceleration[0] = acceleration_function(time_vector[-1])

    # Generate trajectory
    while position[-1] < test_length and velocity[-1] >= 0:
        time_vector = numpy.append(time_vector, [time_vector[-1] + sample_period])
        dt = time_vector[-1] - time_vector[-2]
        acceleration = numpy.append(acceleration, [acceleration_function(time_vector[-1])])
        velocity = numpy.append(velocity, [velocity[-1] + acceleration[-2] * dt])
        if velocity_to_micros(velocity[-2]) == 0:
            position = numpy.append(position, [position[-1]])
        else:
            position = numpy.append(position, [position[-1] +
                numpy.floor(dt * 1e6 / velocity_to_micros(velocity[-2])) /
                    ticks_per_m])
            # position = numpy.append(position, [position[-1] +
            #     micros_to_velocity(velocity_to_micros(velocity[-2])) * dt])
        # break

    # print(time)
    # print(acceleration)
    # print(velocity)
    # print(position)
    print(time_vector.min(), time_vector.max())
    print(acceleration.min(), acceleration.max())
    print(velocity.min(), velocity.max())
    print(position.min(), position.max())

    v_micros = numpy.zeros(velocity.shape)
    for i in range(len(velocity)):
        v_micros[i] = velocity_to_micros(velocity[i])

    # Save trajectory
    numpy.savetxt("input.csv",
        numpy.transpose((time_vector,
                         acceleration,
                         velocity,
                         position,
                         v_micros)),
        delimiter=",")

    # Open serial port
    with serial.Serial(sys.argv[1], int(sys.argv[2])) as output:
        # Output trajectory
        start = time.time()
        for i in range(len(time_vector)):
            now = time.time()
            while now - start < time_vector[i]:
                now = time.time()
                time.sleep(1e-6)
            micros = velocity_to_micros(velocity[i])
            # print(now - start,
            #     time_vector[i],
            #     velocity[i],
            #     velocity_to_micros(velocity[i]),
            #     micros_to_velocity(velocity_to_micros(velocity[i])))
            output.write(str(micros).encode())
            output.flush()
