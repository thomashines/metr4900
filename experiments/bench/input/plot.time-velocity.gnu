#!/usr/bin/env gnuplot

# Output
set terminal svg size 1000, 500 font "HelveticaNeue-Thin,12"
set output "plot.time-velocity.svg"

# Key
set tmargin at screen 0.9
set rmargin at screen 0.95
set bmargin at screen 0.2
set lmargin at screen 0.1
#set key bmargin left
set key at 0.3,-0.1
set key box black
set key horizontal
#set key off

# Labels
set title "Calibrated trajectory" font "HelveticaNeue-Thin,16" offset 0,-0.8
set xlabel "Time (s)"
set ylabel "(m/s)"
set y2label "(us)"

# Axes
set tics out
set grid ytics
set y2tics
set ytics nomirror
#set xrange [0:0.7831]
set y2range [0:2000]

# Plot
set multiplot
plot "input.csv" using 1:3 with lines ls 1 title "Velocity", \
     "input.csv" using 1:5 axes x1y2 with linespoints ls 2 title "Micros", \
