#!/usr/bin/env gnuplot

# Output
set output "data.las.svg"

# Key
set bmargin at screen 0.2
set key bmargin left
set key box black
set key horizontal
#set key off

# Labels
set title "ADC"
set xlabel "Sample"
set ylabel "ADC (0-4096)"

# Axes
set tics out
#set xrange [12500:17500]

# Plot
set multiplot
plot   "data.15.good.csv" using 1:3 with lines ls 1 title "Laser"
