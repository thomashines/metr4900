#!/usr/bin/env python

import numpy
import sys

# Config
spool_diameter = 22E-3
ticks_per_rev = 24
ticks_per_m = ticks_per_rev/(numpy.pi * spool_diameter) # (0.35 ticks/mm)
print("ticks_per_m", ticks_per_m)

# First 50mm: 17 ticks (0.34 ticks/mm)
# Last 50mm: 17 ticks (0.34 ticks/mm)

# First 100mm: 33 ticks (0.33 ticks/mm)
# Last 100mm: 35 ticks (0.35 ticks/mm)

# 250mm: 81, 81, 81 (0.324 ticks/mm)
# 500mm: 166, 164, 165 (0.330 ticks/mm)
# 750mm: 252, 251, 251 (0.335 ticks/mm)

m = [
    [81, 250E-3],
    [165, 500E-3],
    [251, 750E-3],
]

g = (m[1][1] * (m[2][0] ** 2) - m[2][1] * (m[1][0] ** 2)) / \
    ((m[2][0] ** 2) * m[1][0] - (m[1][0] ** 2) * m[2][0])
c = (m[2][1] - g * m[2][0]) / (m[2][0] ** 2)
print("g", g)
print("c", c)

def ticks_to_m(ticks):
    return c * (ticks ** 2) + g * ticks

if __name__ == "__main__":
    # Load Arduino
    arduino = numpy.loadtxt(sys.argv[1], delimiter=",")

    # Convert to real
    real = numpy.zeros(arduino.shape)
    real[:,0] = arduino[:,1] * 1E-6
    real[:,1] = arduino[:,0] / ticks_per_m

    # Save
    numpy.savetxt("real.csv", real, delimiter=",")
