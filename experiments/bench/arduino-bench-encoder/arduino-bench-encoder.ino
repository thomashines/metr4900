#define PIN_HIGH 2
#define PIN_INPUT 3
#define PIN_GND 4

// Path state
// bool go;
volatile unsigned long blocked_us;
volatile unsigned long start_us;
volatile unsigned long blocks;

void setup() {
    // Setup pins
    pinMode(PIN_HIGH, OUTPUT);
    digitalWrite(PIN_HIGH, HIGH);
    pinMode(PIN_INPUT, INPUT);
    pinMode(PIN_GND, OUTPUT);
    digitalWrite(PIN_GND, LOW);

    // Attach interrupt
    attachInterrupt(digitalPinToInterrupt(PIN_INPUT), tick_interrupt, FALLING);

    // Serial
    Serial.begin(115200);

    // Path state
    blocked_us = 0;
    start_us = 0;
    blocks = 0;
}


void loop() {
    if (blocked_us > 0) {
        if (start_us == 0) {
            start_us = blocked_us;
        }
        Serial.print(blocks - 1);
        Serial.print(",");
        Serial.println(blocked_us - start_us);
        blocked_us = 0;
    } else if (Serial.available()) {
        if (Serial.read() == 'y') {
            // Clear state
            blocked_us = 0;
            start_us = 0;
            blocks = 0;
        }
    }
}

void tick_interrupt() {
    blocked_us = micros();
    blocks++;
}
