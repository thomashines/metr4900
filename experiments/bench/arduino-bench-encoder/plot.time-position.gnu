#!/usr/bin/env gnuplot

# Output
set terminal svg size 1000, 500 font "HelveticaNeue-Thin,12"
set output "plot.time-position.svg"

# Key
set tmargin at screen 0.9
set rmargin at screen 0.95
set bmargin at screen 0.2
set lmargin at screen 0.1
#set key bmargin left
set key at 0.3,-0.1
set key box black
set key horizontal
#set key off

# Labels
set title "Calibrated trajectory" font "HelveticaNeue-Thin,16" offset 0,-0.8
set xlabel "Time (s)"
set ylabel "Position (m)"

# Axes
set tics out
set grid ytics
#set xrange [0:0.7831]
#set yrange [0:0.6308]

# Plot
set multiplot
plot "real.csv" using 2:1 with lines ls 4 title "Real position"
