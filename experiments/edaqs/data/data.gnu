#!/usr/bin/env gnuplot

# Output
set output "data.png"

# Key
set bmargin at screen 0.2
set key bmargin left
set key box black
set key horizontal
set key off

# Labels
set title "ADC"
set xlabel "Sample"
set ylabel "ADC (0-4096)"

# Axes
set tics out

# Plot
set multiplot
plot   "data.4.csv" using 1:2 with lines ls 1 title "ADC"
