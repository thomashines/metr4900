#include <Wire.h>

// Include wire
// I2C address of the MPU-6050
// When AD0 = 0
#define MPU_ADDR 0x68
// When AD0 = 1
// #define MPU_ADDR 0x69

// MPU-6050 registers
// PWR_MGMT_1: DEVICE_RESET, SLEEP, CYCLE, -, TEMP_DIS, CLK_SEL[2:0]
#define ADDR_PWR_MGMT_1 0x6B
// ACCEL_CONFIG: AFS_SEL
#define ADDR_ACCEL_CONFIG 0x1C
// ACCEL_XOUT_H
#define ADDR_ACCEL_XOUT_H 0x3B
// ACCEL_YOUT_H
#define ADDR_ACCEL_YOUT_H 0x3D
// ACCEL_ZOUT_H
#define ADDR_ACCEL_ZOUT_H 0x3F

// Intensity pin
#define INTENSITY_PIN A8

// Laser pin
#define LASER_PIN 3

void write_register(int address, int value) {
    // Start transmission
    Wire.beginTransmission(MPU_ADDR);
    // Select register
    Wire.write(address);
    // Set value
    Wire.write(value);
    // End transmission
    Wire.endTransmission(true);
}

void setup() {
    // Set up laser
    pinMode(LASER_PIN, OUTPUT);

    // Start I2C communication
    // Wire.begin();
    // Wire.setClock(400000L);

    // Configure accelerometer

    // Select accelerometer full scale range
    // +-2g
    // write_register(ADDR_ACCEL_CONFIG, 0b11100000);
    // +-4g
    // write_register(ADDR_ACCEL_CONFIG, 0b11101000);
    // +-8g
    // write_register(ADDR_ACCEL_CONFIG, 0b11110000);
    // +-16g
    // write_register(ADDR_ACCEL_CONFIG, 0b11111000);

    // Disable sleep
    // write_register(ADDR_PWR_MGMT_1, 0);

    // Get AFS
    // while (1) {
    //     Wire.beginTransmission(MPU_ADDR);
    //     Wire.write(ADDR_ACCEL_CONFIG);
    //     Wire.endTransmission(false);
    //     Wire.requestFrom(MPU_ADDR, 1, true);
    //     char afs_sel = Wire.read();
    //     Serial.print("AFS_SEL ");
    //     Serial.println(afs_sel);
    //     delay(1000);
    // }


    // Set intensity pin as input
    pinMode(INTENSITY_PIN, INPUT);

    // Start serial communication
    Serial.begin(115200);
}

bool laser_state;
unsigned long us;
int16_t intensity, AcX, AcY, AcZ;

void loop() {
    // Get time
    us = micros();

    // Read analog input
    intensity = analogRead(INTENSITY_PIN);

    // Read data from accelerometer
    // Wire.beginTransmission(MPU_ADDR);
    // X, Y and Z
    // Wire.write(ADDR_ACCEL_XOUT_H);
    // Wire.endTransmission(false);
    // Wire.requestFrom(MPU_ADDR, 6, true);
    // int16_t AcX = (Wire.read() << 8) | Wire.read();
    // int16_t AcY = (Wire.read() << 8) | Wire.read();
    // int16_t AcZ = (Wire.read() << 8) | Wire.read();
    // Y
    // Wire.write(ADDR_ACCEL_YOUT_H);
    // Wire.endTransmission(false);
    // Wire.requestFrom(MPU_ADDR, 2, true);
    // AcY = (Wire.read() << 8) | Wire.read();

    // Read AFS_SEL
    // Wire.beginTransmission(MPU_ADDR);
    // Wire.write(ADDR_ACCEL_CONFIG);
    // Wire.endTransmission(false);
    // Wire.requestFrom(MPU_ADDR, 1, true);
    // AcX = Wire.read();

    // Print data
    Serial.print(us);
    // Serial.println(us);
    Serial.print(",");
    Serial.println(intensity);
    // Serial.print(",");
    // Serial.print(AcX);
    // Serial.print(",");
    // Serial.print(AcY);
    // Serial.println(AcY);
    // Serial.print(",");
    // Serial.println(AcZ);

    // Toggle laser
    if (((us / 1000000) % 20) < 10) {
        if (laser_state == LOW) {
            digitalWrite(LASER_PIN, HIGH);
            laser_state = HIGH;
        }
    } else if (laser_state == HIGH) {
        digitalWrite(LASER_PIN, LOW);
        laser_state = LOW;
    }

    // delay(1);
}
