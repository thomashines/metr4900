// Shafts
shaft_small_d = 10.5;
shaft_large_d = 15.85;

// Bearing
bearing_bolt_d = 4;
bearing_h = 4;
bearing_d = 14;
bearing_ext_d = 8;
bearing_ext_h = 1;

// Clip
clip_h = 16;
clip_w = 43;

// Hinge
hinge_bolt_d = 3;
hinge_h_small = shaft_small_d + 4;
hinge_h_large = shaft_large_d + 4;
hinge_d = 2 * max([shaft_small_d, shaft_large_d]) + 8;

// Join
join_bolt_d = 3;
join_h = 32;

// Pen
pen_h = 32;
pen_bicep_h = 36;
pen_forearm_h = 64;
pen_bolt_d = 3.5;
catch_rotation = 11.6;

// Shroud
shroud_d = 6;
shroud_h = 24;

// Wheels
wheels_h = 18;
wheels_wheel_d = 70;
wheels_wheel_h = 18;
wheels_bearing_d = 13.5;
wheels_bearing_h = 8;
wheels_bearing_y = shaft_large_d / 2 + 2 + wheels_wheel_d / 2 + 2;
