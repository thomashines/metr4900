include <params.scad>;

module hinge_small($fn=16) {
    difference() {
        cylinder(d=hinge_d, h=hinge_h_small);
        translate([0, 0, -1])
        cylinder(d=hinge_bolt_d, h=hinge_h_small + 2);
        translate([(shaft_small_d + hinge_bolt_d) / 2, 0, hinge_h_small / 2])
        rotate([90, 0, 0])
        cylinder(d=shaft_small_d, h=hinge_d * 2, center=true);
    }
}

module hinge_large($fn=16) {
    difference() {
        cylinder(d=hinge_d, h=hinge_h_large);
        translate([0, 0, -1])
        cylinder(d=hinge_bolt_d, h=hinge_h_large + 2);
        translate([(shaft_large_d + hinge_bolt_d) / 2, 0, hinge_h_large / 2])
        rotate([90, 0, 0])
        cylinder(d=shaft_large_d, h=hinge_d * 2, center=true);
    }
}

hinge_small();
/*translate([0, 0, hinge_h_small + 1])*/
translate([hinge_d + 4, 0, 0])
hinge_large();
