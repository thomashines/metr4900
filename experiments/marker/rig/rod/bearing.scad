include <params.scad>;

module bearing($fn=64) {
    difference() {
        union() {
            cylinder(d=bearing_d, h=bearing_h, $fn=6);
            cylinder(d=bearing_ext_d, h=bearing_h + bearing_ext_h);
        }
        translate([0, 0, -1])
        cylinder(d=bearing_bolt_d, h=bearing_h + bearing_ext_h + 2, $fn=$fn);
    }
}

bearing();
