include <params.scad>;

module clip($fn=31) {
    difference() {
        union() {
            // Shaft
            translate([0, 0, 0])
            cylinder(d=shaft_small_d + 4, h=clip_h);

            // To clip
            translate([0, -(shaft_small_d + 4) / 2, 0])
            cube([(shaft_small_d + 4) / 2, shaft_small_d + 4, clip_h]);

            // Clip
            translate([(shaft_small_d + 4) / 2 - 2, -clip_w / 2, 0])
            cube([6, clip_w, clip_h]);
        }

        // Shaft
        translate([0, 0, -1])
        cylinder(d=shaft_small_d, h=clip_h + 2);

        // Clip
        for (i = [0:((clip_w - 5) / 4)]) {
            translate([(shaft_small_d + 4) / 2, -(clip_w - 4) / 2 + 4 * i, -1])
            cube([2, 3, clip_h + 2]);
        }
        translate([(shaft_small_d + 4) / 2 - 3, -(clip_w - 8.3) / 2, clip_h / 2])
        rotate([45, 0, 0])
        translate([0, -1.5, -1.5])
        cube([8, 3, 3]);
        translate([(shaft_small_d + 4) / 2 - 3, (clip_w - 8.3) / 2, clip_h / 2])
        rotate([45, 0, 0])
        translate([0, -1.5, -1.5])
        cube([8, 3, 3]);
    }
}

clip($fn=63);
