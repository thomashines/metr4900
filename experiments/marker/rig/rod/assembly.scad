include <params.scad>;
use <bearing.scad>;
use <clip.scad>;
use <hinge.scad>;
use <join.scad>;
use <pen.scad>;
use <wheels.scad>;

// Bearing
translate([-join_h + wheels_bearing_h, wheels_bearing_y, 0])
rotate([0, -90, 0])
bearing();
translate([-join_h + wheels_bearing_h, -wheels_bearing_y, 0])
rotate([0, -90, 0])
bearing();

// Clip
translate([0, 0, join_h])
clip();

// Join
translate([0, 0, -(shaft_large_d + 4) / 2])
rotate([0, 0, -90])
join();

// Pen
translate([-join_h - wheels_h - pen_h - 2, 0, 0])
rotate([0, 90, 0])
pen();

// Wheels
translate([-join_h - wheels_h, 0, 0])
rotate([0, 90, 0])
wheels();
