include <params.scad>;

module shroud($fn=64) {
    difference() {
        cylinder(d=shroud_d + 4, h=shroud_h);
        translate([0, 0, -1])
        cylinder(d=shroud_d, h=shroud_h + 2);
    }
}

shroud();
