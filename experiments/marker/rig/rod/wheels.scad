include <params.scad>;

module wheels($fn=31) {
    difference() {
        union() {
            // Shaft
            translate([0, 0, 0])
            cylinder(d=shaft_large_d + 4, h=wheels_h);

            // Bearings
            translate([0, wheels_bearing_y, 0])
            cylinder(d=wheels_bearing_d + 4, h=wheels_wheel_h);
            translate([0, -wheels_bearing_y, 0])
            cylinder(d=wheels_bearing_d + 4, h=wheels_wheel_h);

            // Connection
            /*translate([-(wheels_bearing_d + 4) / 2, -wheels_bearing_y, 0])
            cube([(wheels_bearing_d + 4) / 3, wheels_bearing_y * 2, 2]);
            translate([(wheels_bearing_d + 4) / 6, -wheels_bearing_y, 0])
            cube([(wheels_bearing_d + 4) / 3, wheels_bearing_y * 2, 2]);*/
            translate([-(wheels_bearing_d + 4) / 2, -wheels_bearing_y, 0])
            cube([wheels_bearing_d + 4, wheels_bearing_y * 2, 2]);
        }

        // Shaft
        translate([0, 0, -1])
        cylinder(d=shaft_large_d, h=wheels_h + 2);

        // Bearings
        translate([0, wheels_bearing_y, wheels_wheel_h - wheels_bearing_h - 1])
        cylinder(d=wheels_bearing_d, h=wheels_bearing_h + 2);
        translate([0, -wheels_bearing_y, wheels_wheel_h - wheels_bearing_h - 1])
        cylinder(d=wheels_bearing_d, h=wheels_bearing_h + 2);
        translate([0, wheels_bearing_y, -1])
        cylinder(d=wheels_bearing_d - 2, h=wheels_wheel_h + 2);
        translate([0, -wheels_bearing_y, -1])
        cylinder(d=wheels_bearing_d - 2, h=wheels_wheel_h + 2);
    }
}

wheels($fn=63);
