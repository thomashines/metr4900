include <params.scad>;

module pen_pivot($fn=31) {
    difference() {
        union() {
            // Shaft
            translate([0, 0, 0])
            cylinder(d=shaft_large_d + 4, h=pen_h);

            // To axis
            translate([0, -(shaft_large_d + 4) / 2, 0])
            cube([shaft_large_d / 2 + 2 * pen_bolt_d, shaft_large_d + 4, 2 * pen_bolt_d]);

            // Catch
            translate([0, -(shaft_large_d + 4) / 2 + 1, pen_h - 6.5])
            rotate([45, -catch_rotation, 0])
            cube([6, 8, 8], center=true);

            // Spring
            translate([-(shaft_large_d + 4) / 2 + 0.5, 0, 7])
            rotate([0, 45, 0])
            cube([10, 4, 10], center=true);
        }

        // Shaft
        translate([0, 0, -1])
        cylinder(d=shaft_large_d, h=pen_h + 2);

        // Bolt
        translate([shaft_large_d / 2 + pen_bolt_d, 0, pen_bolt_d])
        rotate([90, 0, 0])
        cylinder(d=pen_bolt_d, h=shaft_large_d + 6, center=true);

        // Spring
        translate([-shaft_large_d / 2 - 3.5, 0, 7])
        rotate([90, 0, 0])
        cylinder(d=3, h=shaft_large_d + 6, center=true);
    }
}

module pen_arm($fn=31) {
    difference() {
        union() {
            // Bicep
            translate([-pen_bolt_d, -4, 0])
            cube([2 * pen_bolt_d, 4, pen_bicep_h - pen_bolt_d + 1]);
            translate([0, -4, 0])
            rotate([-90, 0, 0])
            cylinder(d=2 * pen_bolt_d, h=4);

            // Forearm
            translate([-pen_forearm_h + pen_bolt_d, -16, pen_bicep_h - pen_bolt_d])
            cube([pen_forearm_h, 16, 6]);

            // Hook
            translate([-30, 0, pen_bicep_h - pen_bolt_d - 3]) {
                rotate([90, 0, 0])
                cylinder(d=3, h=8);
                translate([0, -2, 2])
                cube([3, 4, 4], center=true);
            }

            // String
            translate([-pen_forearm_h + pen_bolt_d + 3, 0, pen_bicep_h - pen_bolt_d - 1]) {
                rotate([90, 0, 0])
                cylinder(d=6, h=16);
                translate([0, -8, 2])
                cube([6, 16, 4], center=true);
            }
        }

        // Bolt
        translate([0, 1, 0])
        rotate([90, 0, 0])
        cylinder(d=pen_bolt_d, h=8);

        // Forearm
        translate([pen_bolt_d + 1, -8, pen_bicep_h - pen_bolt_d + 10])
        rotate([0, -90, 0])
        cylinder(d=15.5, h=pen_forearm_h + 2);

        // String
        translate([-pen_forearm_h + pen_bolt_d + 3, 1, pen_bicep_h - pen_bolt_d - 1]) {
            rotate([90, 0, 0])
            cylinder(d=3, h=18);
        }
    }
}

pen_pivot($fn=63);

/*translate([shaft_large_d / 2 + pen_bolt_d, -(shaft_large_d + 4) / 2 - 1, pen_bolt_d])
rotate([0, -catch_rotation * sin(360 * $t), 0]) {
    pen_arm($fn=63);
    // Pen
    translate([pen_bolt_d + 1, -8, pen_bicep_h - pen_bolt_d + 10])
    rotate([0, -90, 0])
    cylinder(d=15, h=pen_forearm_h + 2);
}*/
translate([20, -16, 0])
rotate([-90, 0, 0])
pen_arm($fn=63);
