include <params.scad>;

difference() {
    cylinder(d=48, h=32, $fn=6);
    translate([0, 0, -1])
    cylinder(d=32, h=34, $fn=6);
}
