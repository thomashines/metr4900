include <params.scad>;

module join($fn=31) {
    difference() {
        union() {
            // Shafts
            translate([0, (shaft_small_d + 4) / 2, (shaft_large_d + 4) / 2])
            rotate([90, 0, 0])
            cylinder(d=shaft_large_d + 4, h=join_h);
            cylinder(d=shaft_small_d + 4, h=join_h);
        }

        // Shafts
        translate([0, (shaft_small_d + 4) / 2 + 1, (shaft_large_d + 4) / 2])
        rotate([90, 0, 0])
        cylinder(d=shaft_large_d, h=join_h + 2);
        translate([0, 0, -1])
        cylinder(d=shaft_small_d, h=join_h + 2);
    }
}

join();
