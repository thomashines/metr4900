#include <Servo.h>

#define L_EN 38
#define L_DIR A1
#define L_STEP A0
#define R_EN A2
#define R_DIR A7
#define R_STEP A6

#define PEN_UP 135
#define PEN_DOWN 45

#define DIR_LEFT 0
#define DIR_RIGHT 1
#define DIR_DOWN 2
#define DIR_UP 3

Servo pen;
int l_step = 0;
int r_step = 0;
int pos = 0;

void setup() {
    pen.attach(11);
    pinMode(L_EN, OUTPUT);
    pinMode(L_DIR, OUTPUT);
    pinMode(L_STEP, OUTPUT);
    pinMode(R_EN, OUTPUT);
    pinMode(R_DIR, OUTPUT);
    pinMode(R_STEP, OUTPUT);
    digitalWrite(L_EN, LOW);
    digitalWrite(R_EN, LOW);
}

void do_steps(int dir, int count) {
    // Set direction
    switch (dir) {
        case DIR_LEFT:
            digitalWrite(L_DIR, HIGH);
            digitalWrite(R_DIR, HIGH);
            break;
        case DIR_RIGHT:
            digitalWrite(L_DIR, LOW);
            digitalWrite(R_DIR, LOW);
            break;
        case DIR_DOWN:
            digitalWrite(L_DIR, HIGH);
            digitalWrite(R_DIR, LOW);
            break;
        case DIR_UP:
            digitalWrite(L_DIR, LOW);
            digitalWrite(R_DIR, HIGH);
            break;
    }

    // Step
    for (unsigned int s = 0; s < count; s++) {
        digitalWrite(L_STEP, LOW);
        digitalWrite(R_STEP, LOW);
        delay(1);
        digitalWrite(L_STEP, HIGH);
        digitalWrite(R_STEP, HIGH);
        delay(1);
    }
}

#define LINE_COUNT 4
#define LINE_WIDTH 128
#define LINE_LENGTH 512
#define PEN_WIDTH 16

void loop() {
    // Draw some lines
    for (unsigned int l = 0; l < LINE_COUNT; l++) {
        // Do strokes
        pen.write(PEN_DOWN);
        delay(500);
        for (unsigned int s = 0; s < LINE_WIDTH / (2 * PEN_WIDTH); s++) {
            // Go right
            do_steps(DIR_RIGHT, LINE_LENGTH);
            // Go down
            do_steps(DIR_DOWN, PEN_WIDTH);
            // Go left
            do_steps(DIR_LEFT, LINE_LENGTH);
            // Go down
            do_steps(DIR_DOWN, PEN_WIDTH);
        }
        // Go to next line
        pen.write(PEN_UP);
        delay(500);
        do_steps(DIR_DOWN, LINE_WIDTH);
    }

    // Return to start
    do_steps(DIR_UP, 2 * LINE_COUNT * LINE_WIDTH);
}
