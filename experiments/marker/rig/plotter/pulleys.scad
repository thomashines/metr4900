include <params.scad>;

module sprocket() {
    difference() {
        // Body
        union() {
            cylinder(d=sprocket_d + groove_d, h=pulley_h);
            translate([0, 0, -10])
            cylinder(d=stepper_d + 0.5 + 2 * min_h, h=pulley_h + 10);
        }

        // Shaft
        translate([0, 0, -10 - 1])
        difference() {
            cylinder(d=stepper_d + 0.75, h=pulley_h + 10 + 2);
            translate([-stepper_d / 2 + 4.8, -stepper_d / 2, -1])
            cube([stepper_d, stepper_d, pulley_h + 10 + 4]);
        }

        // Groove
        translate([0, 0, pulley_h / 2])
        rotate_extrude()
        translate([sprocket_d / 2 + groove_d / 2, 0, 0])
        circle(d=groove_d);

        // Tie off
        translate([sprocket_d / 2 - 3, 0, 0])
        cylinder(d=2, h=pulley_h + 10 + 2);
    }
}

module idler() {
    difference() {
        // Body
        cylinder(d=idler_d + groove_d, h=pulley_h);
        translate([0, 0, -1])

        // Bolt
        cylinder(d=bolt_d + 0.25, h=pulley_h + 2);

        // Groove
        translate([0, 0, pulley_h / 2])
        rotate_extrude()
        translate([idler_d / 2 + groove_d / 2, 0, 0])
        circle(d=groove_d);
    }
}

/*$fn = 64;*/

for(i = [0 : 3]) {
    translate([i * (idler_d + 4), 0, 0])
    idler();
}

translate([0, idler_d + sprocket_d, pulley_h])
rotate([180, 0, 0])
sprocket();

/*translate([sprocket_d + 4, idler_d + sprocket_d, pulley_h])
rotate([180, 0, 0])
sprocket();*/
