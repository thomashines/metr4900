// Plastic
min_h = 2;

// Rods
rod_d = 4;
rod_x_h = 130;
rod_y_h = 100;

// Pen
pen_d = 16;
pen_h = 130;

// Carriages

// XY
carriage_xy_x = 27;
carriage_xy_y = 26;
carriage_xy_z = 3 * rod_d + 4 * min_h;
rod_dz = rod_d + min_h;

// Actuators
belt_act_l = rod_x_h + rod_y_h;
sprocket_d = 40;
idler_d = 8;
groove_d = 2;
pulley_h = 4;
stepper_d = 5;
bolt_d = 3.5;
stepper_x = 42;
stepper_y = 42;
stepper_z = 39;
stepper_bolt_x = 5;
