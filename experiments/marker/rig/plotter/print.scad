include <params.scad>;
use <carriages.scad>;
use <ends.scad>;
use <hooks.scad>;
use <pulleys.scad>;

$fn = 64;

translate([0, -15, 0])
pen_hook();

translate([30, -15, 0])
pen_hook();

carriage_xy();

/*translate([0, 45, 0])
mirror([0, 1, 0])
carriage_x();

translate([0, 50, 0])
carriage_x();

translate([0, 80, 0])
mirror([0, 1, 0])
idler_end();

translate([0, 85, 0])
idler_end();

translate([50, 0, rod_d / 2 + min_h])
rotate([0, 90, 0])
stepper_end();

translate([50, 90, rod_d / 2 + min_h])
rotate([0, 90, 0])
mirror([0, 1, 0])
stepper_end();*/
