include <params.scad>;
use <carriages.scad>;
use <ends.scad>;
use <hooks.scad>;
use <pulleys.scad>;

$fn = 8;

// Carriage position
x = rod_x_h / 2;
y = rod_y_h / 2;
z = 00;

// Y carriage
translate([x - rod_d / 2 - min_h + carriage_xy_x, y - rod_d / 2 - min_h, -carriage_xy_z / 2])
mirror([1, 0, 0])
carriage_xy();

// X carriages
translate([x - rod_d / 2 - min_h + carriage_xy_x, rod_d / 2 + min_h, -carriage_xy_z / 2])
mirror([0, 1, 0])
mirror([1, 0, 0])
carriage_x();
translate([x - rod_d / 2 - min_h + carriage_xy_x, rod_y_h - rod_d - rod_d / 2 - min_h, -carriage_xy_z / 2])
mirror([1, 0, 0])
carriage_x();

// Idler ends
translate([rod_x_h - rod_d - rod_d / 2 - min_h, rod_d / 2 + min_h, -carriage_xy_z / 2])
mirror([0, 1, 0])
idler_end();
translate([rod_x_h - rod_d - rod_d / 2 - min_h, rod_y_h - rod_d - rod_d / 2 - min_h, -carriage_xy_z / 2])
idler_end();

// Stepper ends
translate([0, -rod_d / 2 - min_h - 5.75, 0])
stepper_end();
translate([0, rod_y_h - rod_d / 2 + min_h + 5.75, 0])
mirror([0, 1, 0])
stepper_end();

// Rods

// X
translate([-rod_d / 2, 0, 0])
rotate([0, 90, 0])
cylinder(d=rod_d, h=rod_x_h);
translate([-rod_d / 2, rod_y_h - rod_d, 0])
rotate([0, 90, 0])
cylinder(d=rod_d, h=rod_x_h);

// Y
translate([0, -rod_d / 2, rod_dz])
rotate([-90, 0, 0])
cylinder(d=rod_d, h=rod_y_h);
translate([x + carriage_xy_x - rod_d - 2 * min_h, -rod_d / 2, -rod_dz])
rotate([-90, 0, 0])
cylinder(d=rod_d, h=rod_y_h);
translate([x + carriage_xy_x - rod_d - 2 * min_h, -rod_d / 2, +rod_dz])
rotate([-90, 0, 0])
cylinder(d=rod_d, h=rod_y_h);
translate([rod_x_h - rod_d, -rod_d / 2, rod_dz])
rotate([-90, 0, 0])
cylinder(d=rod_d, h=rod_y_h);

// Pen
translate([x - rod_d / 2 - min_h + pen_d / 2 + min_h + 1, y - rod_d / 2 - min_h + carriage_xy_y / 2, -20]) {
    cylinder(d=pen_d - 1, h=pen_h);
    translate([0, 0, 55])
    rotate([0, 0, -90])
    pen_hook();
    translate([0, 0, pen_h - 10])
    pen_hook();
}

// Servo
#translate([
    x - rod_d / 2 - min_h + pen_d / 2 + min_h + 1 + pen_d / 2 + min_h,
    y - rod_d / 2 - min_h + carriage_xy_y / 2 - pen_d / 2 + 8 - min_h,
    carriage_xy_z / 2 + 5.25])
union() {
    // Body
    cube([13, 23, 23]);

    // Holes
    translate([0, 4.5, -5])
    cube([13, 2.5, 33]);

    // Gear covers
    translate([6.5, 0, 23 - 6.5])
    rotate([90, 0, 0])
    cylinder(d=13, h=4.5);
    translate([6.5, 0, 23 - 13])
    rotate([90, 0, 0])
    cylinder(d=5.5, h=4.5);

    // Arm
    translate([6.5, -4.5, 23 - 6.5])
    rotate([0, z, 0]) {
        rotate([90, 0, 0])
        cylinder(d=7, h=5);
        translate([-16, -5, -5.5 / 2])
        cube([16, 1.5, 5.5]);
    }
}

// Pulleys

// A
color("Red") {
    // Idlers
    translate([x - rod_d / 2 + bolt_d / 2 + idler_d / 2, -rod_d / 2 - min_h - bolt_d / 2, carriage_xy_z / 2 + 1])
    idler();
    translate([x - rod_d / 2 + bolt_d / 2 + idler_d / 2, rod_y_h - rod_d / 2 + min_h + bolt_d / 2, carriage_xy_z / 2 + 1])
    idler();
    translate([rod_x_h - rod_d / 2 + min_h + bolt_d / 2, -rod_d / 2 - min_h - bolt_d / 2, carriage_xy_z / 2 + 1])
    idler();
    translate([rod_x_h - rod_d / 2 + min_h + bolt_d / 2, rod_y_h - rod_d / 2 + min_h + bolt_d / 2, carriage_xy_z / 2 + 1])
    idler();
    translate([0, -rod_d / 2 - min_h - bolt_d / 2, carriage_xy_z / 2 + 1])
    idler();

    // Sprocket
    translate([0, -rod_d / 2 - min_h - 5.75, carriage_xy_z / 2 + 1])
    translate([-stepper_x / 2 - rod_d / 2 - min_h, stepper_y / 2 - 1, 0])
    sprocket();
}

// B
color("Blue") {
    // Idlers
    translate([x - rod_d / 2 + bolt_d / 2 + idler_d / 2,  -rod_d / 2 - min_h - bolt_d / 2, carriage_xy_z / 2 + pulley_h + 2])
    idler();
    translate([x - rod_d / 2 + bolt_d / 2 + idler_d / 2, rod_y_h - rod_d / 2 + min_h + bolt_d / 2, carriage_xy_z / 2 + pulley_h + 2])
    idler();
    translate([rod_x_h - rod_d / 2 + min_h + bolt_d / 2, -rod_d / 2 - min_h - bolt_d / 2, carriage_xy_z / 2 + pulley_h + 2])
    idler();
    translate([rod_x_h - rod_d / 2 + min_h + bolt_d / 2, rod_y_h - rod_d / 2 + min_h + bolt_d / 2, carriage_xy_z / 2 + pulley_h + 2])
    idler();
    translate([0, rod_y_h - rod_d / 2 + min_h + bolt_d / 2, carriage_xy_z / 2 + pulley_h + 2])
    idler();

    // Sprocket
    translate([0, rod_y_h - rod_d / 2 + min_h + 5.75, carriage_xy_z / 2 + pulley_h + 2])
    mirror([0, 1, 0])
    translate([-stepper_x / 2 - rod_d / 2 - min_h, stepper_y / 2 - 1, 0])
    sprocket();
}

// Tie
color("Purple") {
    translate([x - rod_d / 2 - min_h + min_h + bolt_d / 2, y - rod_d + min_h + bolt_d / 2, carriage_xy_z / 2 + 1])
    #cylinder(d=bolt_d, h=2 * pulley_h + 1);
    translate([x - rod_d / 2 - min_h + min_h + bolt_d / 2, y - rod_d + carriage_xy_y - min_h - bolt_d / 2, carriage_xy_z / 2 + 1])
    #cylinder(d=bolt_d, h=2 * pulley_h + 1);
}
