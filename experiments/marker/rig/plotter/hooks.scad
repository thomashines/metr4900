include <params.scad>;

module pen_hook() {
    difference() {
        union() {
            cylinder(d=15.25 + 2 * min_h, h=min_h);
            translate([0, -min_h - bolt_d / 2, 0])
            cube([15.25 / 2 + 3 * min_h + bolt_d, bolt_d + 2 * min_h, min_h]);
        }
        translate([0, 0, -1])
        cylinder(d=15.25, h=min_h + 2);
        translate([15.25 / 2 + 2 * min_h + bolt_d / 2, 0, -1])
        cylinder(d=bolt_d, h=min_h + 2);
    }
}

pen_hook();
