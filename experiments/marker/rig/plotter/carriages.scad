include <params.scad>;

module carriage_xy() {
    difference() {
        union() {
            // Body
            translate([-7, 0, 0])
            cube([carriage_xy_x + 7, carriage_xy_y, carriage_xy_z]);

            // Servo
            translate([-7, 11, carriage_xy_z])
            cube([13, carriage_xy_y - 11, 5]);
        }

        // Y shafts
        translate([rod_d / 2 + min_h, -1, carriage_xy_z - rod_d / 2 - min_h])
        rotate([-90, 0, 0])
        cylinder(d=rod_d + 0.25, h=carriage_xy_y + 2);
        translate([rod_d / 2 + min_h, -1, rod_d / 2 + min_h])
        rotate([-90, 0, 0])
        cylinder(d=rod_d + 0.25, h=carriage_xy_y + 2);

        // Pen
        translate([carriage_xy_x - pen_d / 2 - min_h - 1, carriage_xy_y / 2, -1])
        cylinder(d=pen_d, h=carriage_xy_z + 2);

        // Pulleys
        translate([carriage_xy_x - bolt_d / 2 - min_h, bolt_d / 2 + min_h, -1])
        cylinder(d=bolt_d, h=carriage_xy_z + 2);
        translate([carriage_xy_x - bolt_d / 2 - min_h, carriage_xy_y - bolt_d / 2 - min_h, -1])
        cylinder(d=bolt_d, h=carriage_xy_z + 2);

        // Servo
        translate([-8, 15.25, carriage_xy_z])
        cube([15, 3, 6]);

        // Servo bolt
        translate([0, -1, carriage_xy_z + 2.25])
        rotate([-90, 0, 0])
        cylinder(d=2, h=carriage_xy_y + 2);
    }
}

module carriage_x() {
    difference() {
        union() {
            // Body
            cube([carriage_xy_x, rod_d + 2 * min_h, carriage_xy_z]);

            // Pulley
            translate([carriage_xy_x - bolt_d - 2 * min_h - idler_d / 2, rod_d + 2 * min_h, 0])
            cube([bolt_d + 2 * min_h, bolt_d + min_h, carriage_xy_z]);
        }

        // Shafts

        // X shaft
        translate([-1, rod_d / 2 + min_h, carriage_xy_z / 2])
        rotate([0, 90, 0])
        cylinder(d=rod_d + 0.25, h=carriage_xy_x + 2);

        // Y shafts
        translate([rod_d / 2 + min_h, -1, carriage_xy_z - rod_d / 2 - min_h])
        rotate([-90, 0, 0])
        cylinder(d=rod_d, h=rod_d + 2 * min_h + 2);
        translate([rod_d / 2 + min_h, -1, rod_d / 2 + min_h])
        rotate([-90, 0, 0])
        cylinder(d=rod_d, h=rod_d + 2 * min_h + 2);

        // Pulleys
        translate([carriage_xy_x - bolt_d / 2 - min_h - idler_d / 2, rod_d + 2 * min_h + bolt_d / 2, -1])
        cylinder(d=bolt_d, h=carriage_xy_z + 2);
    }
}
