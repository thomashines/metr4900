include <params.scad>;

module idler_end() {
    difference() {
        union() {
            // Body
            cube([min_h + rod_d + min_h + bolt_d + min_h, rod_d + 3 * min_h + bolt_d, carriage_xy_z]);
        }

        // Shafts

        // X shaft
        translate([-1, rod_d / 2 + min_h, carriage_xy_z / 2])
        rotate([0, 90, 0])
        cylinder(d=rod_d, h=carriage_xy_x + 2);

        // Y shaft
        translate([rod_d / 2 + min_h, -1, carriage_xy_z - rod_d / 2 - min_h])
        rotate([-90, 0, 0])
        cylinder(d=rod_d, h=rod_d + 3 * min_h + bolt_d + 2);

        // Pulleys
        translate([min_h + rod_d + min_h + bolt_d / 2, rod_d / 2 + min_h + rod_d / 2 + min_h + bolt_d / 2, -1])
        cylinder(d=bolt_d, h=carriage_xy_z + 2);
    }
}

module stepper_end() {
    // Stepper
    /*translate([-stepper_x - rod_d / 2 - min_h, -1, -stepper_z - rod_d / 2 - min_h])
    #cube([stepper_x, stepper_y + 2, stepper_z]);
    translate([-stepper_x / 2 - rod_d / 2 - min_h, stepper_y / 2 - 1, -rod_d / 2 - min_h])
    #cylinder(d=stepper_d, h=19);
    translate([-stepper_x / 2 - rod_d / 2 - min_h, stepper_y / 2 - 1, -rod_d / 2 - min_h])
    #cylinder(d=22, h=2);*/

    difference() {
        union() {
            // Front plate
            translate([-rod_d / 2 - min_h, 0, -rod_d + rod_d / 2 - min_h - 16])
            cube([rod_d + 2 * min_h, stepper_y, 2 * rod_d + 3 * min_h + 16]);

            // Bolt holes
            translate([-2 * stepper_bolt_x - rod_d / 2 - min_h - 1, 0, -rod_d / 2 - min_h])
            cube([2 * stepper_bolt_x + 1, stepper_y, 2 * min_h + rod_d]);
        }

        // Stepper bolts
        translate([-stepper_bolt_x - min_h - rod_d / 2 - 1, stepper_bolt_x, -rod_d / 2 - min_h - 1])
        cylinder(d=bolt_d, h=2 * min_h + rod_d + 2);
        translate([-stepper_bolt_x - min_h - rod_d / 2 - 1, stepper_y - stepper_bolt_x, -rod_d / 2 - min_h - 1])
        cylinder(d=bolt_d, h=2 * min_h + rod_d + 2);

        // Pulley
        translate([0, min_h + bolt_d / 2, -rod_d + rod_d / 2 - min_h - 16 - 1])
        cylinder(d=bolt_d, h=2 * rod_d + 3 * min_h + 16 + 2);

        // Rods
        translate([0, -1, rod_d + min_h])
        rotate([-90, 0, 0])
        cylinder(d=rod_d, h=stepper_y + 2);
        translate([-rod_d / 2 - min_h - stepper_bolt_x + bolt_d / 2 - 10, min_h + rod_d / 2 + 5.75, 0])
        rotate([0, 90, 0])
        cylinder(d=rod_d, h=stepper_y + 2);
    }
}
