close all; clear all;

% Load intensity data
intensity_data = csvread('../data/data.video.2.csv');
intensity_time = intensity_data(:, 1) .* 1E-6;
intensity = intensity_data(:, 2);
intensity_time = intensity_time - 20.002392;

% Load camera time
camera_data = csvread('../data/video.2.csv');
camera_time = camera_data(3:end, 1);
camera = camera_data(3:end, 2);

% Cut
time_start = -1;
time_end = 11;
intensity_time_valid = (intensity_time > time_start) & (intensity_time < time_end);
intensity_time = intensity_time(intensity_time_valid);
intensity = intensity(intensity_time_valid);

figure; hold on;
plot(intensity_time, intensity, 'r');

% Plot lines
for l = 1:size(camera_time, 1)
    disp(l);
    plot([1, 1] .* camera_time(l), [min(intensity), max(intensity)]);
end
