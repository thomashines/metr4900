#!/usr/bin/env gnuplot

# Output
set output "fig-intensity.png"

# Key
set bmargin at screen 0.2
set key bmargin left
set key box black
set key horizontal
set key off

# Labels
set title "Laser reflection intensity"
set xlabel "Time (s)"
set ylabel "Reflection intensity (0-1023)"

# Axes
set tics out

# Plot
set multiplot
plot   "fig-intensity.csv" using 1:2 with lines ls 1 title "intensity"
