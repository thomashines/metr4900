#!/usr/bin/env gnuplot

# Settings
load "../../../settings.gnu"

# Output
set terminal svg size 1000,500 font "Serif,16"
set output "plot.marker.time-laser.svg"

# Labels
set title "Reflection intensity"
set xlabel "Time (s)"
set ylabel "ADC levels"

# Key
set key off

# Axes
#set ytics 250
set grid xtics
set grid ytics

# Plot
plot "fig-intensity.csv" using 1:2 with lines ls 1 title "Reflection intensity"
