Aim:
    Verify markers on inside of steel tube function.
Apparatus:
    Laptop
    USB cable
    Arduino w/ sensor shield
    Markers
        Sharpie
        Whiteboard
        Whiteout?
        Pencil/pacer
    Reaching tool
        1 meter or so
        Marker placement
        Arduino placement
        Camera
Method:
    1. Place markers on inside of steel tube.
    2. Run reflectance sensor over them and observe data.
Risks:
    Laser
        Blinding
    Noise
        Operation involves compressed gas and hydraulic actuators
        Vacate X lab during operation
    Injury from actuated equipment
        Parts of tube are hydraulically actuated
    Working inside X3 test section/dumptank
        Head injury
            Hard objects at head height
        Back injury
            Strain of bending over to reach into tube
    X3 Expansion Tube / X3R Reflected Shock Tunnel Re-commissioning and General Operation
        Trip hazard
            
