# Contents

* /design/
    * container/
        * Container drawings, renders and photographs
    * edaqs/
        * MPLAB IDE project for eDAQS firmware
    * electronics/
        * Electronics drawings, enders and photographs
* /experiments/
    * bench/
        * Data, code and analysis from the bench top experiments
    * edaqs/
        * Data recorded when testing the eDAQS unit with the designed
            electronics
    * marker/
        * Data recorded when testing the checkpoint design in X3's compression
            tube
    * meter/
        * Data, code and analysis from the scale prototype experiments
* /graphics/
    * Images used in assessment
* /poster/
    * Posters designed for the demonstration and the ITEE Innovation Showcase
* /proposal/
    * Project proposal
* /seminar/
    * Seminar
* /thesis/
    * Thesis document source (LyX)
* /x3process/
    * Trajectory reconstruction algorithm
* /x3simulator/
    * X3 compression tube and data acquisition system simulations
* /settings.gnu
    * Shared Gnuplot settings
