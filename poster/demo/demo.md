# Demonstration

## Introduction

1. Description of scenario
  * Sensor package to measure the acceleration of X3's piston
  * Estimate trajectory using the acceleration
  * Deal with integration drift using checkpoints (empirical position
    measurements)
2. Description of bench rig
  * Not to scale demonstration: showing functionality far below X3 conditions
  * Bench rig
    * Length: ~0.79m
    * Peak velocty: ~2.5m/s
    * Peak acceleration:
      * ~30m/s/s on acceleration
      * ~700m/s/s on impact
  * X3
    * Length: ~14m
    * Peak velocity: \>50m/s
    * Peak acceleration:
      * \>100m/s/s on acceleration
      * ~2000m/s/s on deceleration
  * Reduced speed makes checkpoint detection easier
  * Rotary encoder to provide ground truth

## Demonstration

3. Operation
  * Interaction over serial
  * Configure gain, sample rate, triggering settings
  * Start sampling
  * Can be configured to not trigger under low accelerations (e.g. gravity) or
    short duration accelerations (e.g. impacts)
      * Acceleration must stay above trigger level for some period of time
      * 20% of the recorded data is before the system is triggered
  * Laser activates after trigger and when trigger acceleration is met
  * Data extracted over serial
4. Post processing
  * Run x3process.py script
