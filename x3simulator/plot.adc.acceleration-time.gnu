#!/usr/bin/env gnuplot

# Settings
load "../settings.gnu"

# Output
set terminal svg size 1000,500 font "Serif,16"
set output "plot.adc.acceleration-time.svg"

# Key
set key off

# Labels
set title "Simulation output"
set xlabel "Sample number"
set ylabel "Accelerometer (ADC levels)"

# Axes
set yrange [0:4096]
set ytics 0,512,4096
set grid xtics
set grid ytics

# Plot
set multiplot
plot "data.csv" using 1:2 with lines ls 3 title "Accelerometer"
