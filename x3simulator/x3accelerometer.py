#!/usr/bin/env python

import matplotlib.pyplot as pyplot
import numpy
import sys

def apply_noise(acceleration, settings):
    # Select a mean
    mean = (numpy.random.rand(1)[0] - 0.5) * (2E+3)
    print("additive noise mean", mean)

    # Generate a normal array of additive noise
    additive_noise = numpy.random.normal(mean, 50, acceleration.shape)

    # Generate a normal array of multiplicitive noise
    multiplicitive_noise = numpy.random.normal(1, 0.3, acceleration.shape)

    # Return noisy signal
    return (acceleration + additive_noise) * multiplicitive_noise

def apply_accelerometer(time, acceleration, settings):
    # Apply frequency response
    samples = time.shape[0]
    sample_rate = 1/(time[1] - time[0])

    # Get FFT of acceleration
    fft_acceleration = numpy.fft.fft(acceleration)
    fft_frequencies = numpy.fft.fftfreq(samples, d=1/sample_rate)

    # Get frequency response at FFT frequencies
    fft_response = numpy.interp(fft_frequencies, settings[0][:, 0],
        settings[0][:, 1])

    # Flip frequency response across nyquist frequency
    fft_middle = round(samples / 2)
    fft_response[fft_middle:] = numpy.flipud(fft_response[:fft_middle])

    # Apply response
    fft_acceleration *= fft_response

    # Reverse fft
    accelerometer = numpy.fft.ifft(fft_acceleration).real

    # Apply transverse sensitivity
    accelerometer += 9.81 * settings[2]

    # Determine non-linearity
    cubic_component = settings[1] / (numpy.absolute(accelerometer).max() ** 2)

    # Apply non-linearity
    accelerometer += cubic_component * (accelerometer ** 3)
    non_linear_component = cubic_component * (accelerometer ** 3)

    # Apply sensitivity
    accelerometer = settings[3] * accelerometer

    # Apply excitation
    accelerometer *= settings[4]

    # Return result
    return accelerometer

def digipot_resistance(n):
    Rab = 100E+3
    Rw = 75
    count = 256
    Raw = Rab * (1 - n/count) + Rw
    Rwb = Rab * (n/count) + Rw
    return Raw, Rwb

def apply_in_amp(accelerometer, settings):
    return accelerometer * settings[8](settings[7],
        *digipot_resistance(settings[5])) + settings[6]

def apply_adc(in_amp, settings):
    return numpy.floor(in_amp * (settings[10] / settings[9]))

def apply_all(time, acceleration, settings):
    # Noise
    noisy_acceleration = apply_noise(acceleration, settings)

    # Accelerometer
    accelerometer = apply_accelerometer(time, noisy_acceleration, settings)

    # Apply in amp
    in_amp = apply_in_amp(accelerometer, settings)

    # ADC
    adc = apply_adc(in_amp, settings)

    return adc

if __name__ == "__main__":
    # Load input
    data = None
    with open("input.csv", "rb") as input_file:
        data = numpy.loadtxt(input_file, delimiter=",")
    if data is None:
        sys.exit(1)

    # Split input
    time = data[:, 0]
    acceleration = data[:, 1]
    velocity = data[:, 2]
    position = data[:, 3]

    # Data acquisition system parameters
    settings = [
        numpy.array([
                [   0,   20,  100, 1000, 3000, 5000, 7000],
                [1.00, 1.00, 1.00, 1.00, 1.03, 1.07, 1.13],
            ], numpy.float64).transpose(), # 0, frequency response, Hz, ratio
        0.01, # 1, non-linearity, ratio
        0.03, # 2, transverse sensitivity, ratio
        ((0.15 * 1E-3) / 10) / 9.81, # 3, sensitivity, 1/(m/s/s)
        3.3, # 4, excitation, V
        135, # 5, digipot setting, integer
        3.3/2, # 6, in amp reference
        5, # 7, in amp base gain, ratio
        lambda g, r1, r2: g + g * (r2 / r1), # 8, in amp gain, ratio
        3.3, # 9, ADC max voltage, V
        2 ** 12, # 10, ADC levels, integer
        [13, 13.02, 13.04, 13.06], # 11, checkpoint positions, m
    ]

    # Get adc reading
    adc = apply_all(time, acceleration, settings)

    # Get sample number
    sample_number = numpy.arange(0, time.shape[0])

    # Get checkpoints
    checkpoints_list = []
    for n in range(sample_number.shape[0] - 1):
        for x in settings[11]:
            if position[n] <= x <= position[n + 1] or \
                    position[n + 1] <= x <= position[n]:
                checkpoints_list.append([time[n], x, n])

    # Mock laser
    laser = numpy.full(time.shape, settings[10] - 1, dtype=numpy.int64)
    for checkpoint in checkpoints_list:
        laser[checkpoint[2] - 1:checkpoint[2] + 1] = 0

    # Only select first stroke
    end_index = position.argmax()
    end_time = time[end_index]
    print(end_index, end_time)

    # Print stats
    print("acceleration", acceleration[:end_index].min() / 9.81,
        acceleration[:end_index].max() / 9.81)
    print("velocity", velocity[:end_index].min(), velocity[:end_index].max())
    print("adc", adc[:end_index].min(), adc[:end_index].max())

    # Plot output
    pyplot.plot(time[:end_index], adc[:end_index])
    pyplot.show()

    # Save to csv file
    with open("data.csv", "wb") as input_file:
        numpy.savetxt(input_file,
            numpy.stack([sample_number, adc, laser], axis=1).astype(
                numpy.int64)[:end_index, :],
            fmt="%d",
            header="sample,adc",
            delimiter=",")
    with open("checkpoints.truth.csv", "wb") as input_file:
        numpy.savetxt(input_file,
            numpy.array([c[:2] for c in checkpoints_list if c[0] < end_time]),
            header="time,position",
            delimiter=",")
    with open("truth.csv", "wb") as input_file:
        numpy.savetxt(input_file,
            numpy.stack([time, acceleration, position], axis=1)[:end_index, :],
            header="time,acceleration,position",
            delimiter=",")
