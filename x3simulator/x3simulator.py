#!/usr/bin/env python

from scipy.optimize import minimize
import matplotlib.pyplot as pyplot
import numpy

# X3 specs
x3_length = 14
x3_diameter = 0.5
x3_area = numpy.pi * (x3_diameter / 2) ** 2
piston_mass = 280

# Sampling
zero_duration = 0.1
total_duration = 0.5
rate = 1/35E-6

# Minimise variable
initial_settings = [
    0.9151, # loss factor
    1.6935, # initial_piston_drive_volume
    1.6935, # initial_piston_drive_pressure, MPa
    0.1769, # initial_driver_pressure, MPa
]

# Goals
min_acceleration = -2000*9.81
max_driver_pressure = 30E+6
max_velocity = 100

def get_driver_pressure(settings, position):
    return settings[3] * 1E+6 * x3_length / (x3_length - position)

def get_piston_drive_pressure(settings, position):
    return settings[2] * 1E+6 * settings[1] / (settings[1] + position * x3_area)

def get_acceleration(settings, position, time):
    if time < zero_duration:
        return 0
    return (get_piston_drive_pressure(settings, position) -
            get_driver_pressure(settings, position)) * x3_area / piston_mass

def get_trajectory(settings):
    # Number of samples
    time = numpy.arange(0, total_duration, 1/rate)
    samples = time.shape[0]

    # Driver pressure
    driver_pressure = numpy.zeros(samples)

    # Piston state starts at 0
    acceleration = numpy.zeros(samples)
    velocity = numpy.zeros(samples)
    position = numpy.zeros(samples)

    # Do integration
    driver_pressure[0] = get_driver_pressure(settings, position[0])
    acceleration[0] = get_acceleration(settings, position[0], time[0])
    for n in range(samples - 1):
        dt = time[n + 1] - time[n]
        driver_pressure[n + 1] = get_driver_pressure(settings, position[n])
        acceleration[n + 1] = get_acceleration(settings, position[n], time[n])

        # Apply loss factor as a friction
        acceleration[n + 1] += -settings[0] * velocity[n]

        velocity[n + 1] = velocity[n] + \
            (acceleration[n] + acceleration[n + 1]) * (dt / 2)
        position[n + 1] = position[n] + \
            (velocity[n] + velocity[n + 1]) * (dt / 2)

    return time, acceleration, velocity, position, driver_pressure

# Objective function
def cost(settings):
    # Get trajectory
    time, acceleration, velocity, position, driver_pressure = get_trajectory(
        settings)

    # Compare to goals
    return ((acceleration.min() - min_acceleration) / 20) ** 2 + \
           (velocity.max() - max_velocity) ** 2

if __name__ == "__main__":
    print("x3simulator.py")

    # Settings limits
    settings_bounds = numpy.array([[s/10, s*10] for s in initial_settings],
        numpy.float64)

    # Determine settings
    minimise_result = minimize(cost, initial_settings,
        bounds=settings_bounds,
        options={"disp": True},
        callback=lambda xk: print(xk))
    settings = minimise_result["x"]
    # settings = initial_settings
    print("settings", settings)

    # Get trajectory
    time, acceleration, velocity, position, driver_pressure = get_trajectory(
        settings)

    print("time", time.min(), time.max())
    print("driver_pressure", driver_pressure.min() * 1E-6,
        driver_pressure.max() * 1E-6, "MPa")
    print("acceleration", acceleration.min() / 9.81, acceleration.max() / 9.81,
        "g")
    print("velocity", velocity.min(), velocity.max())

    # Plot acceleration vs. time
    pyplot.title("Piston acceleration vs. time")
    pyplot.xlabel("Time (s)")
    pyplot.ylabel("Acceleration (m/s/s)")
    pyplot.plot(time, acceleration)
    pyplot.show()

    # Save to csv file
    with open("input.csv", "wb") as input_file:
        numpy.savetxt(input_file,
            numpy.stack([time, acceleration, velocity, position], axis=1),
            header="time,acceleration,velocity,position",
            delimiter=",")
