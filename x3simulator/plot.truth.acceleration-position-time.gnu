#!/usr/bin/env gnuplot

# Settings
load "../settings.gnu"

# Output
set terminal svg size 1000,500 font "Serif,16"
set output "plot.truth.acceleration-position-time.svg"

# Key
set key left center

# Labels
set title "Simulation input trajectory (ground truth)"
set xlabel "Time (s)"
set ylabel "Acceleration (m/s/s)"
set y2label "Position (m)"

# Axes
set yrange [-22000:2000]
set ytics 2000
set y2range [-4:20]
set y2tics 2
set ytics nomirror
set y2tics
set grid xtics
set grid ytics

# Plot
set multiplot
plot "truth.csv" using 1:2           with lines ls 3 title "Acceleration", \
     "truth.csv" using 1:3 axes x1y2 with lines ls 2 title "Position"
