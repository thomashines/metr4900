#!/usr/bin/env gnuplot

# Settings
load "../settings.gnu"

# Output
set terminal svg size 1000,500 font "Serif,16"
set output "plot.sim.time-position-truth.svg"

# Key
set key top left

# Labels
set title "Reconstructed trajectory vs. true trajectory"
set xlabel "Time (s)"
set ylabel "Position (m)"
set y2label "Error (m)"

# Axes
set xtics 0.025
set y2tics
set yrange [-2:18]
set ytics -2,2,18
#set y2range [0:0.01]
set ytics nomirror
set grid xtics
set grid ytics

# Plot
set multiplot
plot       "error.csv" using 1:3           with  lines ls 2 lc rgb "#BB645B4B" lw 8 title "True position", \
           "error.csv" using 1:2           with  lines ls 2                         title "Estimated position", \
           "error.csv" using 1:4 axes x1y2 with  lines ls 6                         title "Error", \
     "checkpoints.csv" using 1:2           with points ls 1                         title "Checkpoints"
